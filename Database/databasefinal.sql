CREATE DATABASE  IF NOT EXISTS `poodle_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `poodle_db`;
-- MariaDB dump 10.19  Distrib 10.5.10-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: poodle_db
-- ------------------------------------------------------
-- Server version	10.5.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `isPublic` tinyint(2) NOT NULL DEFAULT 0,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `userId` int(11) NOT NULL,
  `isDeleted` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_courses_users_idx` (`userId`),
  CONSTRAINT `fk_courses_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (44,'Learn Ethical Hacking From Scratch','Become an ethical hacker that can hack computer systems like black hat hackers and secure them like security experts.',1,'2021-08-27 14:13:40',22,0),(47,'The Complete Cyber Security Course : Network Security!','Volume 2 : Network Security, WiFi Security, WiFi Hackers, Firewalls, Wireshark, Secure Networking. + Password Managers',1,'2021-08-27 14:14:19',22,0),(49,'Life Coaching Certification Course (Beginner to Advanced)','A comprehensive online training that delivers in-depth understanding of the key elements of the Life Coaching profession',1,'2021-08-27 14:15:10',22,0),(50,'Photography Masterclass: A Complete Guide to Photography','The Best Online Professional Photography Class: How to Take Amazing Photos for Beginners & Advanced Photographers',0,'2021-08-27 14:15:52',21,0),(51,'Premiere Pro CC for Beginners: Video Editing in Premiere','Learn how to edit videos in Adobe Premiere Pro with these easy-to-follow Premiere Pro video editing tutorials.',0,'2021-08-27 14:16:01',21,0),(52,'Pianoforall - Incredible New Way To Learn Piano & Keyboard','Learn Piano in WEEKS not years. Play-By-Ear & learn to Read Music. Pop, Blues, Jazz, Ballads, Improvisation, Classical',0,'2021-08-27 14:16:20',21,0),(53,'Complete Guitar Lessons System - Beginner to Advanced','All-in-one Guitar Course, Fingerstyle Guitar, Blues Guitar, Acoustic Guitar, Electric Guitar & Fingerpicking Guitarra',0,'2021-08-27 14:16:30',21,0),(54,'Watercolor in the Woods','\n A Beginner\'s Guide to Painting the Natural World',0,'2021-09-02 21:14:03',29,0);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrolled`
--

DROP TABLE IF EXISTS `enrolled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enrolled` (
  `courseId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`courseId`,`userId`),
  KEY `fk_courses_has_users_users1_idx` (`userId`),
  KEY `fk_courses_has_users_courses1_idx` (`courseId`),
  CONSTRAINT `fk_courses_has_users_courses1` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_courses_has_users_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrolled`
--

LOCK TABLES `enrolled` WRITE;
/*!40000 ALTER TABLE `enrolled` DISABLE KEYS */;
INSERT INTO `enrolled` VALUES (50,20),(51,20),(52,20),(53,20),(54,20);
/*!40000 ALTER TABLE `enrolled` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opinions`
--

DROP TABLE IF EXISTS `opinions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opinions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `content` varchar(255) NOT NULL,
  `isRequest` tinyint(2) NOT NULL DEFAULT 0,
  `createdOn` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opinions`
--

LOCK TABLES `opinions` WRITE;
/*!40000 ALTER TABLE `opinions` DISABLE KEYS */;
INSERT INTO `opinions` VALUES (1,'steffankostov@yahoo.de','I like it a lot.',0,'2021-08-22 11:47:47'),(2,'steffankostov@yahoo.de','I have a problem with the register form.',1,'2021-08-22 11:48:19'),(4,'emilia.georgieva@gmail.com','Should I register if I would like see courses content?',1,'2021-08-24 15:37:16');
/*!40000 ALTER TABLE `opinions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Teacher'),(2,'Student');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `content` mediumtext NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `availableFrom` datetime DEFAULT NULL,
  `courseId` int(11) NOT NULL,
  `orderPosition` int(11) DEFAULT NULL,
  `isDeleted` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_sections_courses1_idx` (`courseId`),
  CONSTRAINT `fk_sections_courses1` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (26,'Beginner - The CORE - Module 1','<iframe src=\"https://www.youtube.com/embed/BBz-Jyr23M4\" height=\"315\" width=\"560\"></iframe>','2021-08-27 15:41:15','2021-08-26 00:00:00',53,3,0),(28,'Beginner - The CORE - Module 2','<h3>Learning BUTTER by BTS by EAR (plus 5 TIPS to help YOU do the same)</h3><br /><br /><iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Mkp3OrpU_ZU\"></iframe>','2021-08-27 15:46:30','2021-08-26 00:00:00',53,2,0),(29,'Beginner - The CORE - Module 3','<h3>In this video I discuss the most necessary elements to correctly choose and purchase an acoustic guitar.</h3><br /><br /><iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/6Jxz9F3CYuo\"></iframe>','2021-08-27 15:48:28','2021-08-26 00:00:00',53,1,0),(30,'Beginner - The CORE - Module 4','<h3>Play \'Bitter Sweet Symphony\' by The Verve with FOUR simple CHORDS</h3>\n<br /><br />\n<iframe src=\"https://www.youtube.com/embed/rSuutGEvhEM\" height=\"315\" width=\"560\"></iframe>','2021-08-27 15:53:33','2021-08-26 00:00:00',53,5,0),(31,'Beginner - The CORE - Module 5','<h3>Play \'Tainted Love\' on GUITAR - EASY riff &amp; simple chords</h3>\n<br /><br />\n<iframe src=\"https://www.youtube.com/embed/xHUyVouCAzk\" height=\"315\" width=\"560\"></iframe>','2021-08-27 15:54:30','2021-12-15 00:00:00',53,4,0),(33,'Lesson 1 - Introduction','<h3>Intermediate Piano Course, Lesson 1 || Scale Revision and a Piece</h3><iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/312-_CUICv8\"></iframe>','2021-08-27 16:11:15','2021-08-26 00:00:00',52,3,0),(34,'Lesson 2 - Rhythm','<h3>Intermediate Piano Course, Lesson 2 || More \"Chaconne\", Pedalling and Rubato\n</h3>\n<iframe src=\"https://www.youtube.com/embed/kkodF7TJUrs\" height=\"315\" width=\"560\"></iframe>','2021-08-27 16:14:26','2021-08-26 00:00:00',52,2,0),(35,'Lesson 3 - Seeing music patterns','<h3>Intermediate Piano Course, Lesson 3 || Next Level Scales!\n</h3>\n<iframe src=\"https://www.youtube.com/embed/omlAz7Bedfc\" height=\"315\" width=\"560\"></iframe>','2021-08-27 16:15:14','2021-08-26 00:00:00',52,1,0),(36,'Lesson 4 - Basic Musical Notation','<h3>Intermediate Piano Course, Lesson 4 || Hello, Mozart!\n</h3>\n<iframe src=\"https://www.youtube.com/embed/XX6DvFP-lFY\" height=\"315\" width=\"560\"></iframe>','2021-08-27 16:15:41','2021-08-26 00:00:00',52,5,0),(37,'Lesson 5 - The C Family Chords','<h3>Intermediate Piano Course, Lesson 1 || Scale Revision and a Piece</h3><iframe src=\"https://www.youtube.com/embed/zmGDGh4vblw\" height=\"315\" width=\"560\"></iframe>','2021-08-27 16:17:43','2021-08-18 00:00:00',52,4,0),(38,'Introduction to Adobe Premiere Pro','<h3>\nAdobe Photoshop CC 2017: Tutorial for Beginners - Lesson 1</h3>\n<iframe src=\"https://www.youtube.com/embed/gUc6giUUEQs\" height=\"315\" width=\"560\"></iframe>','2021-08-27 16:57:49','2021-08-26 00:00:00',51,3,0),(39,'Premiere Pro Basics','<h3>IPhotoshop: How To Cut Out an Image</h3><iframe src=\"https://www.youtube.com/embed/Hw0-SehGcgg\" height=\"315\" width=\"560\"></iframe>','2021-08-27 16:59:33','2021-08-26 00:00:00',51,2,0),(40,'Video Editing Basics','<h3>Photoshop CS6/CC: How To Blend Two Images Together</h3><iframe src=\"https://www.youtube.com/embed/US3NZc_pmSI\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:00:32','2021-08-26 00:00:00',51,1,0),(41,'Adding Style to your videos','<h3>TOP 10 Photoshop Shortcuts</h3><iframe src=\"https://www.youtube.com/embed/wk960TDAyLU\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:01:57','2021-08-26 00:00:00',51,5,0),(42,'Audio Editing in Premiere','<h3>Photoshop: How To Resize Audio File</h3><iframe src=\"https://www.youtube.com/embed/E76YIao7lJo\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:02:45','2021-08-26 00:00:00',51,4,0),(43,'Welcome to the Course','<h3>Learn Photography - Simple, Practical </h3><iframe src=\"https://www.youtube.com/embed/ujaCbzLwuB8\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:05:29','2021-08-26 00:00:00',50,3,0),(44,'Exposure','<h3>Get off \'Auto\' mode - Photography Basics</h3><iframe src=\"https://www.youtube.com/embed/My1Z2_e4EPI\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:06:36','2021-08-26 00:00:00',50,2,0),(45,'Composition','<h3>Top 10 Composition Tips</h3><iframe src=\"https://www.youtube.com/embed/5V4uuNdmRHc\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:07:26','2021-08-26 00:00:00',50,1,0),(46,'Focus and depth of Field','<h3>Perfect Exposure &amp; Metering made EASY</h3><iframe src=\"https://www.youtube.com/embed/jmY3Nac26yc\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:08:12','2021-08-26 00:00:00',50,5,0),(47,'Camera Anatomy','<h3>Camera Anatomy</h3>\n<iframe src=\"https://www.youtube.com/embed/EdxKl5np9KE\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:11:15','2021-08-26 00:00:00',50,4,0),(48,'Welcoming introduction','<h3>Life Coaching Structure To Elevate Your Coaching Sessions | Christine Hassler</h3><iframe src=\"https://www.youtube.com/embed/ilu61IUs0IA\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:13:10','2021-08-26 00:00:00',49,3,0),(49,'A Propper Personal Vision','<h3>First Coaching Session Structure For New Clients</h3>\n<iframe src=\"https://www.youtube.com/embed/AnRAwY4GD-4\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:14:06','2021-08-26 00:00:00',49,2,0),(50,'The purpose of Life Coaching','<h3>Sample Life Coaching Session in Under 7 Minutes\n</h3><iframe src=\"https://www.youtube.com/embed/rITJeAIvTxY\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:15:10','2021-08-26 00:00:00',49,1,0),(51,'Raising Personal Standards','<h3>Principles of Coaching with Confidence</h3><iframe src=\"https://www.youtube.com/embed/LakB3jkMnAc\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:15:49','2021-08-26 00:00:00',49,5,0),(52,'Past  Future Exercise','<h3>Life Purpose Coaching</h3><iframe src=\"https://www.youtube.com/embed/apvIpRyTZf0\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:17:21','2021-08-26 00:00:00',49,4,0),(53,'Introduction','<h3>The Basics of Cybersecurity</h3><iframe src=\"https://www.youtube.com/embed/b2XCUEq385w\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:23:32','2021-08-26 00:00:00',47,3,0),(54,'Goals and Learning objectives','<h3>NIST Security Framework - Cybersecurity Crash Course For Small Organizations</h3><iframe src=\"https://www.youtube.com/embed/6zvl3_FZem4\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:24:23','2021-08-26 00:00:00',47,2,0),(55,'Routers and Ports','<h3>Getting Into Cyber Security</h3><iframe src=\"https://www.youtube.com/embed/Kx4y9c7w2JQ\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:25:42','2021-08-26 00:00:00',47,1,0),(56,'Firewalls','<h3>Introduction To Cyber Security</h3><iframe src=\"https://www.youtube.com/embed/z5nc9MDbvkw\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:26:16','2021-08-26 00:00:00',47,5,0),(57,'Network Attacks','<h3>how Hackers SNiFF (capture) network traffic </h3><iframe src=\"https://www.youtube.com/embed/-rSqbgI7oZM\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:27:07','2021-08-26 00:00:00',47,4,0),(58,'Teaser Hacking A Windows','<h3>Using My Python Skills To Punish Credit Card Scammers</h3><iframe src=\"https://www.youtube.com/embed/StmNWzHbQJU\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:29:25','2021-08-26 00:00:00',44,3,0),(59,'Setting up a hacking lab','<h3>Full Ethical Hacking Course - Network Penetration Testing for Beginners</h3><iframe src=\"https://www.youtube.com/embed/3Kq1MIfTWCE\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:30:01','2021-08-26 00:00:00',44,2,0),(60,'Linux Basics','<h3>Ethical Hacking with Linux Basics</h3><iframe src=\"https://www.youtube.com/embed/GFP3Q4g9PuY\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:31:15','2021-08-26 00:00:00',44,1,0),(61,'Network Hacking','<h3>How To Become A Hacker In 2021</h3><iframe src=\"https://www.youtube.com/embed/W6vcd2gJw3Q\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:31:42','2021-08-26 00:00:00',44,5,0),(62,'Pre Connection Attacks','<h3>Pre connection unethical Attacks</h3><iframe src=\"https://www.youtube.com/embed/lZAoFs75_cs\" height=\"315\" width=\"560\"></iframe>','2021-08-27 17:32:26','2021-08-26 00:00:00',44,4,0),(63,'Beginner- Module 1','<iframe src=\"https://www.youtube.com/embed/zoun01krIfg\" height=\"315\" width=\"560\"></iframe>','2021-09-02 21:21:53','2021-09-06 00:00:00',54,1,0),(64,'Beginner- Module 2','<iframe src=\"https://www.youtube.com/embed/1Fgkwcym4j4\" height=\"315\" width=\"560\"></iframe>','2021-09-02 21:29:43','2021-09-06 00:00:00',54,2,0);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `roleId` int(11) NOT NULL,
  `isDeleted` tinyint(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_roles_idx` (`roleId`),
  CONSTRAINT `fk_users_roles` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (14,'steffankostov@abv.bg','Stefan','Kostov','$2b$10$2G2vVYSMDarpqHzSHlqEh.CzZTeiY3EeHZ3iCDwtHp7bj6q/jew9G','2021-08-20 10:57:56',1,0),(15,'olgaandreeva@abv.bg','Olga','Andreeva','$2b$10$IUgMWjWT3UoIMcRXJH4k8erzCXM1.VGU0sKtVy1xdOmLA0eqPy8QC','2021-08-20 10:58:56',1,0),(16,'milenkostov@abv.bg','Milen','Kostov','$2b$10$KL5xAV4ymXyEfnh0/8.h6uF03SDEeVurv7EsG5mNrBaZTgMi1PuH.','2021-08-20 10:59:23',2,0),(17,'stiliyanhristov@abv.bg','Stiliyan','Hristov','$2b$10$dRBjiG1/ObESgA5HWUlfnO.owIrSJtjeyQrMZmkhKNrq3BcmjmpCa','2021-08-20 10:59:52',2,0),(18,'luchezarpavlov@abv.bg','Luchezar','Pavlov','$2b$10$CnnTgLi8jz0SCXxQsyGK4ekhQ19k/m.RY4iYlrAI.vVJAlJO7r4Iy','2021-08-20 11:00:18',2,0),(19,'marktwain@abv.bg','Mark','Twain','$2b$10$vkPc7oJTv.ef8KM6BEAnF.RGwEzCswYedq.R6MMKzPRHDWMLJoGWW','2021-08-20 11:00:37',1,0),(20,'ivanpetkov@abv.bg','Ivan','Petkov','$2b$10$5MGQ.uQ3TtdQ9LeD/4irteSUA9c3P36BAMp9Y.9/2nOmEIN.hTuK2','2021-08-21 12:49:25',2,0),(21,'ivangeorgiev@abv.bg','Ivan','Georgiev','$2b$10$46ttTu2rlcw5Klk7m/XiMe4Oiv1dIziOZpCv1fMnX9etRu69s7P1O','2021-08-24 09:51:10',1,0),(22,'petersavovsky@abv.bg','Peter','Savovsky','$2b$10$CD784aUvQ5.ZjkUbGdVMCenq7x6vEA2mtYg/N1j9aFgJ5J77rr2dK','2021-08-27 14:08:21',1,0),(23,'irina.valkova@gmail.com','Irina','Valkova','$2b$10$k3B8qXw.ODoiVPArzo8zyuOPG5948aWktPZZUkaysX3ohvWRQzwmy','2021-09-02 18:43:51',2,0),(24,'alexander.kulev65@yahoo.com','Alexander','Kulev','$2b$10$SINjpQYAr9UWfOEyt3eyou8Gq/vvjahBugdp9ZrnoAPKobysfUKHq','2021-09-02 18:47:54',2,0),(25,'antoniogeorgiev@abv.bg','Antonio','Georgiev','$2b$10$wZhJ2SKEMja9gQMhkDAzwOpQt.Z4A9lEgwXA0e36Gw3IAVxpmLG7m','2021-09-02 18:48:49',2,0),(26,'veselametodieva93@abv.bg','Vesela','Metodieva','$2b$10$QV6JuTBW8IR/jHUtYYNzpuftZd1l1n79DC/3.t4Kr1Sm0DT1XNsxC','2021-09-02 18:50:11',2,0),(27,'elenaatanasova@gmail.com','Elena','Atanasova','$2b$10$NrE7H9JjhWEE7eEvk0TICOKgLTrx9f.OEz9XLu3iYfpe8ANZAM3d2','2021-09-02 18:51:08',2,0),(28,'kostadin.dimitrov@yahoo.com','Kostadin','Dimitrov','$2b$10$GWYKejl6xRs.8Evwt4uNr.euqVShaLlCo7/.qbVoq9OzBkahvm2se','2021-09-02 18:52:15',2,0),(29,'olga.kambitova@gmail.com','Olga','Andreeva','$2b$10$w0L/JTVRtV5Qwt32J6bEXOTcIeRXH7.HHTc4o7hcAAdZY2HTiJVrW','2021-09-02 19:17:31',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-02 21:40:13
