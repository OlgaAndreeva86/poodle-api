# Poodle API

### 1. Description

Fullstack single-page application, which enables users to browse and get enrolled to different courses, based on their preferences.

In order to experience a course, the user should be logged in, which differentiates him from a guest, who can only see the courses and see the public courses. When the user is enrolled to a course, he/she can browse the sections in the course too .

The courses that are being browsed are created by the teachers. The app has an administration part for managing users,courses and sections.

This is a student project, part of the final assignment of the Telerik Alpha JavaScript Track.


### 2. Creators

- Stefan Kostov - @stefan_kostov
- Olga Andreeva- @OlgaAndreeva86

### 3. Core Back-end Technologies
 - Node.js
 - Express
 - MariaDB
  
### 4. Core Front-end Technologies
 - React
 - Material-UI
 - Bootstrap



### 5. Features
#### Public

 - Home page with introducing media.

![Home 2](Poodle-Frontend/poodle-frontend/screenshots/homepublic.png)
 - Register
![Register](Poodle-Frontend/poodle-frontend/screenshots/register.png)
 - Login
![Login](Poodle-Frontend/poodle-frontend/screenshots/login.png)
 - Contact page
![Contact us](Poodle-Frontend/poodle-frontend/screenshots/contactus.png)



#### Private
 
 - 
![create a course](Poodle-Frontend/poodle-frontend/screenshots/createcourse.png)
![create a section](Poodle-Frontend/poodle-frontend/screenshots/createsection.png)
![edit courses](Poodle-Frontend/poodle-frontend/screenshots/editcourse.png)
![edit sections](Poodle-Frontend/poodle-frontend/screenshots/editsection.png)
![all students view](Poodle-Frontend/poodle-frontend/screenshots/allstudents.png)
![opinions and requests page](Poodle-Frontend/poodle-frontend/screenshots/opinions.png)
![FAQ](Poodle-Frontend/poodle-frontend/screenshots/faq.png)
![User profile](Poodle-Frontend/poodle-frontend/screenshots/myprofile.png)
 - Edit user profile details and password
![Edit user details](Poodle-Frontend/poodle-frontend/screenshots/editprofile.png)
![See courses of teacher](Poodle-Frontend/poodle-frontend/screenshots/coursesofteacher.png)



### 8. Back-end Setup

Prerequisites
 - [Node.js](https://nodejs.org/en/) installation
 - [MariaDB](https://mariadb.org/download/) installation

1. Go inside the `Poodle-Backend` folder.
2. Run `npm install` or `npm i` to restore all dependencies.
3. You can setup a local database 
 - Import the database schema from the **databasefinal** file in the **Database** folder. 
4. The project is currently linked to a remote database. To connect to a local database update the config.js file in the server root folder with the following configuration:
   ```js
   PORT=5555
   HOST=localhost
   DB_PORT=3306
   USER=root // Replace your MariaDB database username
   PASSWORD=1234 // Replace your MariaDB database password 
   DATABASE=poodle_db
   ```
5. After that, there are a few scripts you can run:

- `npm start` - will run the current version of the code and will **not** reflect any changes done to the code until this command is executed again
- `npm run start:dev` - will run the code in *development* mode, meaning every change done to the code will trigger the program to restart and reflect the changes made

### 9. Front-end Setup

1. Go inside the `poodle-frontend` folder.
2. Run `npm install` or `npm i` to restore all dependencies.
3. After that you can run:
#### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />

