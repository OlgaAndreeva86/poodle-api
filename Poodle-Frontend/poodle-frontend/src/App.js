import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Navi from './Main/Navi';
import Home from './Main/Home';
import CoursesList from './Courses/CourseList/CourseList';
import Course from './Courses/Course/Course';
import CreateCourseForm from './Courses/CreateCourse/CreateCourse';
import CreateSectionForm from './Sections/Sections/CreateSection/CreateSection';
import Header from "./Authentication/Header/Header"
import CourseUpdate from './Courses/CourseUpdate/CourseUpdate'; 
import User from './Users/UsersView/UsersView';
import UsersList from './Users/UsersList/UsersList';
import About from './About/About';
import LoginForm from './Forms/LoginForm/LoginForm';
import RegisterForm from './Forms/RegisterForm/RegisterForm';
import LearnMore from './About/LearnMore';
import UpdateSection from './Sections/SectionUpdate/SectionUpdate';
import EnrollStudents from './Users/EnrollStudents/EnrollStudents';
import AllEnrolledStudents from './Users/AllEnrolledStudents/AllEnrolledStudents';
import { useState } from 'react';
import { getUserFromToken } from './Authentication/Utils/token';
import { AuthContext } from './Authentication/Providers/AuthContext';
import SingleSection from './Sections/Sections/Sections/SingleSection/SingleSection';
import EditUser from './Users/EditUser/EditUser';
import GuardedRoute from './Authentication/Providers/GuardedRoute'
import Sections from './Sections/Sections/Sections/Sections';
import Faq from './FAQs/FAQs';
import AllOpinions from './Users/AllOpinions/AllOpinions';


function App() {

  const [authValue, setAuthValue] = useState ({
    isLoggedIn: !!getUserFromToken(localStorage.getItem('token')),
    user: getUserFromToken(localStorage.getItem('token')),
  });

  return (
    <div className="App">
      <header className="App-header">
        <BrowserRouter>
        <AuthContext.Provider value={{...authValue, setUser: setAuthValue}}>
        <Navi />
        <Switch>
        <Redirect path="/" exact to="/home" />
        <Route exact path="/home" component= {Home} />
        <Route exact path="/auth/login" component= {LoginForm} />
        <Route exact path="/auth/register" component= {RegisterForm} />
        <GuardedRoute exact path="/myprofile" auth={authValue.isLoggedIn} component= {Header} />
        <Route exact path="/about" component= {About} />
        <Route exact path="/learnMore" component= {LearnMore} />
        <Route exact path="/courses" component= {CoursesList} />
        <GuardedRoute exact path="/courses/:id" auth={authValue.isLoggedIn} component= {Course} />
        <GuardedRoute exact path="/create" auth={authValue.isLoggedIn} component= {CreateCourseForm} />
        <GuardedRoute exact path="/courses/:id/edit" auth={authValue.isLoggedIn} component= {CourseUpdate} />
        <GuardedRoute exact path="/courses/:id/create" auth={authValue.isLoggedIn} component= {CreateSectionForm} />
        <GuardedRoute exact path="/courses/:id/sections/:sectionId/edit" auth={authValue.isLoggedIn} component= {UpdateSection} />
        <GuardedRoute exact path="/courses/:id/sections" auth={authValue.isLoggedIn} component= {Sections} />
        <GuardedRoute exact path="/users/:id/courses" auth={authValue.isLoggedIn} component= {User} />
        <GuardedRoute exact path="/courses/:id/enroll/students" auth={authValue.isLoggedIn} component= {EnrollStudents} />
        <GuardedRoute exact path="/courses/:id/enrolled/students" auth={authValue.isLoggedIn}  component= {AllEnrolledStudents} />
        <GuardedRoute exact path="/users" auth={authValue.isLoggedIn} component= {UsersList} />
        <GuardedRoute exact path="/users/opinions" auth={authValue.isLoggedIn} component= {AllOpinions} />
        <GuardedRoute exact path="/courses/:id/sections/:sectionId" auth={authValue.isLoggedIn} component= {SingleSection} />
        <GuardedRoute exact path="/users/:id/edit" auth={authValue.isLoggedIn} component= {EditUser} />
        <Route exact path="/faqs" component= {Faq} />
        </Switch>
        </AuthContext.Provider>
        </BrowserRouter>
      </header>
    </div>
  );
}

export default App;
