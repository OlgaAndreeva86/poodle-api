import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../Providers/AuthContext";
import { useHistory } from "react-router";
import { BASE_URL } from "../../Common/constants";
import User from "../../Users/UsersView/UsersView";
import Student from "../../Users/StudentsView/StudentsView";
import { Collapse } from "@material-ui/core";
import { Col, Container, Row, Button } from "reactstrap";
import profileBig from "../../Resources/profileBig.png";
import { ListGroupItem } from "reactstrap";
import { ListGroup } from "reactstrap";
import "./Header.css";

const Header = () => {
  const { user, setUser } = useContext(AuthContext);
  const history = useHistory();
  let [checked, setChecked] = useState(false);

  const goBack = () => history.push("/home");

  const refreshPage = () => {
    window.location.reload();
  };

  const triggerLogout = () => {
    setUser({ isLoggedIn: false, user: null });
    localStorage.removeItem("token");
    goBack();
  };

  const [userInfo, setUserInfo] = useState(null);

  useEffect(() => {
    fetch(`${BASE_URL}/users/${user?.sub}`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((result) => result.json())
      .then((data) => setUserInfo(data));
  }, [user?.sub]);

  return (
    <div className="Header page-wrapper">
      <Button
        className="mt-5"
        color="primary"
        variant="primary btn-block"
        type="submit"
        onClick={() => {
          triggerLogout();
          refreshPage();
        }}
      >
        Log out
      </Button>

      <Container className="mt-5">
        <Row>
          <Col lg={4} md={6} sm={12} className="text-center mt-5 p-3">
            {userInfo ? (
              <ListGroup className="list-group-flush">
                <ListGroupItem>
                  {" "}
                  <h3 className="firstName">Hi, {userInfo.firstName}</h3>
                </ListGroupItem>
                <ListGroupItem>
                  {user?.sub ? (
                    <h4 className="role">
                      We hope your experience as a {user.role} in Poodle goes
                      amazing as always !
                    </h4>
                  ) : null}
                </ListGroupItem>
              </ListGroup>
            ) : null}

            <div className="text-left mt-3">
              <Button
                color="secondary"
                className="btn-lg"
                type="submit"
                onClick={() => history.push(`/users/${user.sub}/edit`)}
              >
                Edit profile
              </Button>
            </div>
          </Col>

          <Col lg={8} md={6} sm={12}>
            <img className="w-100" src={profileBig} alt="" />
          </Col>
        </Row>
      </Container>

      <div>
        {checked === false ? (
          <Button
            color="secondary"
            className="btn-lg"
            type="submit"
            onClick={() => {
              setChecked(true);
            }}
          >
            Show more
          </Button>
        ) : (
          <Button
            color="secondary"
            className="btn-lg"
            type="submit"
            onClick={() => {
              setChecked(false);
            }}
          >
            Show less
          </Button>
        )}
        <br></br>
        <Collapse in={checked} {...(true ? { timeout: 3000 } : {})}>
          {user?.role === "Teacher" ? <User /> : <Student />}
        </Collapse>
      </div>
    </div>
  );
};

export default Header;
