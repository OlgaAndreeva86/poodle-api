import decode from "jwt-decode";

export const getUserFromToken = (token) => {
    try {
      return decode(token);
    } catch {
      localStorage.removeItem(token);
    }
    return null;
  };

  