import React, { useState, useEffect } from "react";
import Editor from "./Editor";
import { BASE_URL } from "./../../../Common/constants";
import Loader from "../../../Loader/Loader";
import { useHistory } from "react-router";
import DOMPurify from "dompurify";
import { Button } from "reactstrap";
import "./CreateSection.css";

const CreateSectionForm = (props) => {

  const [orderArray, setOrderArray] = useState([]);
  const [section, setSection] = useState({
    title: "",
    content: "",
    availableFrom: "",
    orderPosition: "",
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const history = useHistory();

  const { id } = props.match.params;

  const config = { ADD_TAGS: ["iframe"], KEEP_CONTENT: false };

  const goBack = () => history.push(`/courses/${id}`);

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}/sections`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.message) {
          return setOrderArray([0]);
        }
        setOrderArray(data.map((d) => d.orderPosition));
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id]);


  const createSection = (section) => {
    if (
      section.title.length < 3 || !section.content.length || !section.availableFrom
    ) {
      return alert(
        `You should input title, content and date! The title must contain at least three characters!`
      );
    }
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}/sections`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        ...section,
        orderPosition: Math.max(...orderArray) + 1,
      }),
    })
      .then((response) => {
        setLoading(false);
        return response.json();
      })
      .then(() => goBack())
      .catch((error) => {
        setLoading(false);
        setError(error.message);
      });
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div className="page-wrapper">
      <div className="pane top-pane">
        <label className="createTitle">Create Title</label>
        <br></br>

        <input
          type="text"
          id="title"
          value={section.title}
          onChange={(e) => setSection({ ...section, title: e.target.value })}
        ></input>
        <div className="createEditor">
          <Editor
            language="xml"
            displayName="HTML Content"
            value={section.content}
            onChange={(e) =>
            setSection({ ...section, content: DOMPurify.sanitize(e, config) })
          }
          />
        </div>
        <label className="createAvailable" id="available-from">
          This section is available from:
        </label>
        <br></br>
        <input
          type="date"
          value={section.availableFrom}
          onChange={(e) =>
            setSection({ ...section, availableFrom: e.target.value })
          }
        ></input>
      </div>

      <div className="pane">
        <iframe
          srcDoc={section.content}
          title="output"
          sandbox="allow-scripts"
          frameBorder="0"
          width="100%"
          height="100%"
        />
      </div>
      <Button color="secondary" onClick={() => createSection(section)}>
        Create Section{" "}
      </Button>
    </div>
  );
};

export default CreateSectionForm;
