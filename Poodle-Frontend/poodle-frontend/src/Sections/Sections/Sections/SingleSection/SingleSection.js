import React, { useState, useEffect } from "react";
import { BASE_URL } from "../../../../Common/constants";
import Loader from "../../../../Loader/Loader";
import { useHistory } from "react-router";
import Parser from "html-react-parser";
import { Button } from "reactstrap";
import "./SingleSection.css";

 const SingleSection = (props) => {
  const [section, setSection] = useState({
    title: "",
    content: "",
    availableFrom: "",
    createdOn: "",
  });
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const { id, sectionId } = props.match.params;
  const history = useHistory();

  const goBack = () => history.push(`/courses/${id}`);

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}/sections/${sectionId}`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((response) => response.json())
      .then((data) => setSection(data))
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id, sectionId]);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div className="page-wrapper section">
      <div className="individual-section">
        <h1 id="title">{section.title}</h1>
        {Parser(section.content)}
        <div>
          <Button
            color="secondary"
            type="submit"
            id="btn"
            onClick={() => goBack()}
          >
            {" "}
            Go back{" "}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default SingleSection;
