import React, { useContext, useState, useEffect } from "react";
import { BASE_URL } from "../../../Common/constants";
import Loader from "../../../Loader/Loader";
import { useHistory } from "react-router";
import { AuthContext } from "../../../Authentication/Providers/AuthContext";
import { Link } from "react-router-dom";
import { IconButton } from "@material-ui/core";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import moment from "moment";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import { Button } from "reactstrap";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import "./Sections.css";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    textAlign: "left",
    marginLeft: "300px",
    marginRight: "300px",
    marginBottom: "30px",
    opacity:'0.85'
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 20,
    fontFamily: "Nunito",
    fontWeight:'bolder'
  },
  pos: {
    marginBottom: 12,
  },
});

const Sections = ({ id }) => {
  const [sections, setSections] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const { user } = useContext(AuthContext);
  const history = useHistory();

  const classes = useStyles();

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}/sections`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((response) => response.json())
      .then((data) => {
        setSections(data);
      })

      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id]);

  const order = (updateData, id) => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}/sections/order`, {
      method: "POST",
      headers: {
        "Content-Type": "Application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(updateData),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data.message) {
          throw Error(data.message);
        }
        setSections(data);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };

  const handleDeleteSection = (id, sectionId) => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}/sections/${sectionId}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((_) => {
        const updatedSections = sections.filter(
          (section) => section.id !== sectionId
        );
        setSections(updatedSections);
        history.push(`/courses/${id}`);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div>
      {user?.role === "Student" && (
        <div>
          {sections.length ? (
            <div>
              {sections.map((section) => (
                <div key={section.id} className="AllSections">
                  <br></br>
                  <div>
                    {moment(section.availableFrom).isBefore(
                      moment().format("YYYY-MM-DD")
                    ) ? (
                      <Card className={classes.root}>
                        <CardContent>
                          <Typography
                            className={classes.title}
                            color="textSecondary"
                            gutterBottom
                          >
                            <Link
                              id="link"
                              to={`/courses/${id}/sections/${section.id}`}
                            >
                              {section.title}{" "}
                            </Link>
                           </Typography>
                            <h6 className="sectionDate">
                            created:{" "}
                            {moment(new Date(section.createdOn)).fromNow()}
                            </h6>
                          </CardContent>
                          </Card>
                       ) : (
                      <Card className={classes.root}>
                        <CardContent>
                          <Typography
                            className={classes.title}
                            color="textSecondary"
                            gutterBottom
                          >
                            {section.title}
                          </Typography>
                            <h5 className="message">
                              Sorry, this section is restricted until: {" "}
                              {new Date(
                                section.availableFrom
                              ).toLocaleDateString()}
                            </h5>
                            <h6 className="sectionDate">
                            created:{" "}
                            {moment(new Date(section.createdOn)).fromNow()}
                            </h6>
                        </CardContent>
                      </Card>
                    )}
                  </div>
                </div>
              ))}
            </div>
          ) : null}
        </div>
      )}

      {user?.role === "Teacher" && (
        <div>
          <Button
            color="secondary"
            className="btn-lg"
            type="button"
            onClick={() => history.push(`/courses/${id}/create`)}
          >
            Create Section
          </Button>
          {sections.length ? (
            <div>
              <DragDropContext
                onDragEnd={(params) => {
                  const order1 = params.source.index;
                  const order2 = params.destination?.index;
                  order2 && order({ order1, order2 }, id);
                }}
              >
                <div className="list-container">
                  <Droppable droppableId="droppable-1">
                    {(provided, _) => (
                      <div ref={provided.innerRef} {...provided.droppableProps}>
                        <br />
                        <ul>
                          {sections.map((section) => (
                            <Draggable
                              key={section.id}
                              draggableId={section.id.toString()}
                              index={section.orderPosition}
                             >
                              {(provided, snapshot) => (
                                <div
                                  ref={provided.innerRef}
                                  {...provided.draggableProps}
                                >
                                  <div {...provided.dragHandleProps}>
                                    <Card className={classes.root}>
                                      <CardContent>
                                        <Typography
                                          style={{ fontFamily: "Nunito" }}
                                          className={classes.title}
                                          color="textSecondary"
                                          gutterBottom
                                        >
                                          <Link
                                            className="sectionTitle"
                                            id="link"
                                            to={`/courses/${id}/sections/${section.id}`}
                                          >
                                            {section.title}{" "}
                                          </Link>
                                        </Typography>
                                        <IconButton
                                          onClick={() =>
                                            history.push(
                                              `/courses/${id}/sections/${section.id}/edit`
                                            )
                                          }
                                        >
                                          <EditOutlinedIcon className="EditIcon" />
                                        </IconButton>
                                        <br></br>
                                        <IconButton
                                          onClick={() =>
                                            handleDeleteSection(id, section.id)
                                          }
                                        >
                                          <DeleteOutlineIcon
                                            color="secondary"
                                            className="DeleteIcon"
                                          />
                                        </IconButton>
                                      </CardContent>
                                      <Typography
                                        style={{ fontFamily: "Nunito" }}
                                        variant="h5"
                                        component="h2"
                                      >
                                        <h6 className="message">
                                          Available for students from:{" "}
                                          {new Date(
                                            section.availableFrom
                                          ).toLocaleDateString()}
                                        </h6>
                                      </Typography>
                                    </Card>
                                  </div>
                                </div>
                              )}
                            </Draggable>
                          ))}
                          {provided.placeholder}
                        </ul>
                      </div>
                    )}
                  </Droppable>
                </div>
              </DragDropContext>
            </div>
          ) : null}
        </div>
      )}
    </div>
  );
};

export default Sections;
