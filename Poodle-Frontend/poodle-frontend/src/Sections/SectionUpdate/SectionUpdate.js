import React, { useState, useEffect } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { useHistory } from "react-router";
import moment from "moment";
import background from "../../Resources/updateee.jpg";
import DOMPurify from "dompurify";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "reactstrap";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import "./SectionUpdate.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 645,
    margin: "0 auto",
    borderRadius: "50px",
  },
  media: {
    height: 140,
  },
});

const UpdateSection = (props) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const { id, sectionId } = props.match.params;
  const history = useHistory();
  const classes = useStyles();

  const config = { ADD_TAGS: ["iframe"], KEEP_CONTENT: false };

  const [updateSection, setUpdateSection] = useState({
    title: "",
    content: "",
    availableFrom: "",
    orderPosition: "",
  });

  const goBack = () => history.push("/courses");

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}/sections/${sectionId}`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.message) {
          throw Error(data.message);
        }
        setUpdateSection({
          title: data.title,
          content: data.content,
          availableFrom: moment(data.availableFrom).format("YYYY-MM-DD"),
          orderPosition: data.orderPosition,
        });
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id, sectionId]);

  const update = (sectionData) => {
    if (
      sectionData.title.length < 3 || !sectionData.content.length || !sectionData.orderPosition
    ) {
      return alert(
        ` The title must contain at least three characters/The description must contain at least 10 characters!`
      );
    }
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}/sections/${sectionId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "Application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(sectionData),
    })
      .then((result) => result.json())
      .catch((error) => setError(error.message))
      .finally(() => {
        setLoading(false);
        history.push(`/courses/${id}`);
      });
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div
      className="update-course-form page-wrapper"
      style={{
        backgroundImage: `url(${background})`,
        position: "relative",
        backgroundSize: "cover",
        height:'100vh'
      }}
    >
      <div>
        <Card className={classes.root}>
          <CardMedia
            className={classes.media}
            image="https://inhabitat.com/wp-content/blogs.dir/1/files/2019/03/shutterstock_521176768ban.jpg"
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              <label className="updateTitle" htmlFor="title">
                {" "}
                Title:{" "}
              </label>
              <br />
              <textarea
                className="updateTitleField"
                rows="2"
                cols="80"
                value={updateSection.title}
                onChange={(e) =>
                  setUpdateSection({ ...updateSection, title: e.target.value })
                }
                npm
                star
              ></textarea>
            </Typography>

            <label className="updateContent" htmlFor="content">
              {" "}
              Content:{" "}
            </label>
            <br />
            <textarea
              className="updateContentField"
              rows="5"
              cols="80"
              value={updateSection.content}
              onChange={(e) =>
                setUpdateSection({
                  ...updateSection,
                  content: DOMPurify.sanitize(e.target.value, config),
                })
              }
              npm
              star
            ></textarea>

            <br />
            <label className="availableFrom" htmlFor="content">
              {" "}
              Available From:{" "}
            </label>
            <br />
            <input
              className="input"
              type="date"
              value={moment(updateSection.availableFrom).format("YYYY-MM-DD")}
              onChange={(e) =>
                setUpdateSection({
                  ...updateSection,
                  availableFrom: e.target.value,
                })
              }
            />
            <br />
          </CardContent>
        </Card>
        <br />
        <Button
          color="secondary"
          className="m-1"
          type="submit"
          onClick={() => update(updateSection)}
        >
          {" "}
          Update{" "}
        </Button>
        <Button
          color="info"
          className="m-1"
          type="submit"
          onClick={() => goBack()}
        >
          {" "}
          Go back{" "}
        </Button>
      </div>
    </div>
  );
};

export default UpdateSection;
