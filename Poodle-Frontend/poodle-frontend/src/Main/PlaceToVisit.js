import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ImageCard from './ImageCard';
import places from "../Main/places";
import useWindowPosition from './useWindowPosition';


const useStyles = makeStyles((theme) => ({
root:{
minHeight:'100vh',
display:'flex',
justifyContent:'center',
alignItems:'center',
},
}));

 const PlaceToVisit = () => {
    const classes = useStyles();
    const checked =  useWindowPosition('top');

    return (
        <div className={classes.root} id='place-to-visit'>
            <ImageCard place={places[0]} checked={checked} />
            <ImageCard place={places[1]} checked={checked}  />
        </div>
        
    )
 }
    
 export default PlaceToVisit;