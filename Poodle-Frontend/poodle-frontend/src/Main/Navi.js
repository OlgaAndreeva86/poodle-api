import React, {useState, useContext} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { useHistory } from "react-router";
import { AuthContext } from '../Authentication/Providers/AuthContext';
import { AppBar } from '@material-ui/core';
import './Navi.css';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    color: theme.palette.success.dark
  },
  appBar: {
    padding: '5px',
    backgroundColor: 'transparent'
  },
  tab: {
    fontSize: '20px',
    fontWeight: 'bolder'
  }
}));

 export default function CenteredTabs() {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const { user } = useContext(AuthContext);
   
  const history = useHistory();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <AppBar elevation={0} className={classes.appBar}>
     
        <Paper elevation={4} style={{backgroundColor:'#linear-gradient(90deg, rgba(47,227,97,1) 0%, rgba(34,207,91,1) 35%, rgba(255,236,4,1) 100%)'}} className={classes.root}>
          <Tabs value={value} onChange={handleChange} indicatorColor="primary" textColor="inherit" centered>
            <Tab className={`${classes.tab} tab`} label="Home" onClick={() => history.push("/home")} />
            
            {!user ? <Tab className={`${classes.tab} tab`} label="Login" onClick={() => history.push("/auth/login")}/> : null} 
      
            {!user ? <Tab className={`${classes.tab} tab`} label="Sign up" onClick={() => history.push("/auth/register")}/> : null} 

            <Tab className={`${classes.tab} tab`} label="All Courses" onClick={() => history.push("/courses")} />
            
            {user && user.role === 'Teacher' ? <Tab className={`${classes.tab} tab`} label="All Students" onClick={() => history.push("/users")} /> : null}

            {user && user.role === 'Teacher' ? <Tab className={`${classes.tab} tab`} label="All Opinions/Requests" onClick={() => history.push("/users/opinions")} />  : null}
    
            {user ? <Tab className={`${classes.tab} tab`} label="My Profile" onClick={() => history.push("/myprofile")} /> : null}
      
            <Tab className={`${classes.tab} tab`} label="Contact us" onClick={() => history.push("/about")} />

        </Tabs>
        </Paper>
      </AppBar>

    </div>
    
  );
}
