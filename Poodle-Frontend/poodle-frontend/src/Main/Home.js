import React from "react";
import background from "../Resources/backgroundhome.jpg";
import {makeStyles} from "@material-ui/core/styles";
import { CssBaseline } from "@material-ui/core";
import Top from "./Top";
import PlaceToVisit from "./PlaceToVisit";


const useStyles = makeStyles((theme) => ({
 root: {
  minHeight: '100vh',
  backgroundSize:"cover",
  backgroundImage: `url(${background})`,
  position: "relative",
  backgroundSize: "cover",
 }
 }));

const Home = () => {

const classes = useStyles();

 return (
  <div className={classes.root}>
  <CssBaseline/>
  <Top/>
  <PlaceToVisit />
  </div>
 )
}

export default Home;