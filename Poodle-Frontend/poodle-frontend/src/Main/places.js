

 const places = [ 
    {
        title:'We are not about the profit',
    description:'Poodle is proud to say that 100% of our profit goes into reinvesting in the cause of caring for nature. Come, join the family and be one of us!',
    imageUrl:"https://images.pexels.com/photos/7242976/pexels-photo-7242976.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    time: 1500,
    },
    {
        title:'We are conscious about nature',
    description:'Our community relies on 100% online learning materials, which helps us and our 1000+ members to be saving roughly 65 tons of paper per year!',
    imageUrl:"https://images.pexels.com/photos/612892/pexels-photo-612892.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
    time: 1500,
    },
 ];

 export default places; 