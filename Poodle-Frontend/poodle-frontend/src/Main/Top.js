import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { AppBar, IconButton, Toolbar, Collapse } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Link as Scroll } from "react-scroll";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { useHistory } from "react-router";
import { getUserFromToken } from "../Authentication/Utils/token";
import { Button } from "reactstrap";
import { BASE_URL } from "../Common/constants";
import "./Top.css";
import "../Authentication/Header/Header.css";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    fontFamily: "Nunito",
  },
  appbar: {
    background: "none",
  },
  appbarWrapper: {
    width: "80%",
    margin: "0 auto",
  },
  appbarTitle: {
    flexGrow: "1",
  },
  icon: {
    color: "#fff",
    fontSize: "3rem",
  },
  colorText: {
    color: "#5AFF3D",
  },
  container: {
    textAlign: "center",
  },
  title: {
    color: "#fff",
    fontSize: "4.5rem",
    textAlign: "center",
  },
  goDown: {
    color: "#fff",
    fontSize: "6rem",
  },
  po: {
    color: "#fff",
  },
}));

export default function Top() {
 
  const [user, setUser] = useState( getUserFromToken(localStorage.getItem("token")));
  const classes = useStyles();
  const [checked, setChecked] = useState(false);
  const [userInfo, setUserInfo] = useState(null);
  const history = useHistory();
  const goBack = () => history.push("/home");

  const triggerLogout = () => {
    setUser({ isLoggedIn: false, user: null });
    localStorage.removeItem("token");
    goBack();
  };

  const refreshPage = () => {
    window.location.reload();
  };

  useEffect(() => {
    setChecked(true);
  }, []);

  useEffect(() => {
    fetch(`${BASE_URL}/users/${user?.sub}`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((result) => result.json())
      .then((data) => setUserInfo(data));
  }, [user?.sub]);

  return (
    <div className={classes.root} id="top">
      <AppBar
        style={{ margin: "50px" }}
        className={classes.appbar}
        elevation={0}
      >
        <Toolbar className={classes.appbarWrapper}>
          {user ? (
            <div>
              {" "}
              <IconButton style={{ position: "absolute", right: "10px" }}>
                <a href="/myprofile">
                  <AccountCircleIcon className={classes.icon} />
                </a>
              </IconButton>
              <Button
                className="logOut"
                color="dark"
                type="submit"
                onClick={() => {
                  triggerLogout();
                  refreshPage();
                }}
              >
                Log out
              </Button>
            </div>
          ) : null}
        </Toolbar>
        <h1 style={{textAlign: 'center', marginLeft:'10px'}}>
          Find all your learning materials in one place <br/> Join the env-loving Poodle family
        </h1>
      </AppBar>

      <Collapse
        in={checked}
        {...(checked ? { timeout: 1000 } : {})}
        collapsedHeight={50}
      >
        <div className={classes.container}>
          <Scroll to="place-to-visit" smooth={true}>
            <IconButton>
              <h1 className="learnMore">Learn more</h1>

              <ExpandMoreIcon className={classes.goDown} />
              <br></br>
            </IconButton>
          </Scroll>
        </div>
      </Collapse>
    </div>
  );
}
