import React from 'react';
import { useState } from 'react';
import './FAQs.css';
import { Button, Row } from 'reactstrap';
import img from "../Resources/faq.png"
import { Col } from 'reactstrap';


const Faq  = () => {

  const [zeroClicked, setZeroClicked] = useState(false);
  const [firstClicked, setFirstClicked] = useState(false);
  const [secondClicked, setSecondClicked] = useState(false);
  const [thirdClicked, setThirdClicked] = useState(false);
  const [fourthClicked, setFourthClicked] = useState(false);
  const [fifthClicked, setFifthClicked] = useState(false);
  const [sixthClicked, setSixthClicked] = useState(false);
 
  const zeroClick = (e) => {
    setZeroClicked(!zeroClicked)
  }
  const firstClick = (e) => {
    setFirstClicked(!firstClicked)
  }
  const secondClick = (e) => {
    setSecondClicked(!secondClicked)
  }
  const thirdClick = (e) => {
    setThirdClicked(!thirdClicked)
  }
  const fourthClick = (e) => {
    setFourthClicked(!fourthClicked)
  }
  const fifthClick = (e) => {
    setFifthClicked(!fifthClicked)
  }
  const sixthClick = (e) => {
    setSixthClicked(!sixthClicked)
  }

  return(
    <div className='page-wrapper'>
       <Col lg={8} md={6} sm={12}>
       <Row><div className='title'>FAQs</div></Row>
       <Row>
       <img className="w-50" src={img} alt=""/>
       </Row>
       </Col>
        <Button color="info" size="lg" block onClick={ () =>  zeroClick()}>When was Poodle actually founded?</Button> 
        <br></br>
        {zeroClicked ? <h3 className="faq" >A team of two passionate JS developers founded poodle in August 2021.</h3> : null}
        <br></br>
        <Button color="info" size="lg" onClick={ () =>  firstClick()}>What is the goal of Poodle?</Button> 
        <br></br>
        {firstClicked ? <h3 className="faq">We don't want to erase books and written work from the face of earth. We just believe that a big portion of the written learning materials could be enjoyed online.</h3> : null}
        <br></br>
        <Button color="info" size="lg" onClick={ () =>  secondClick()}>Where is the financing of Poodle coming from? </Button> 
        <br></br>
        {secondClicked ? <h3 className="faq" >We have a full time Patreon page and we offer a premium monthly subscription which gives you access to even more courses.</h3> : null}
        <br></br>
        <Button color="info" size="lg" onClick={ () =>  thirdClick()}>Where are the headquarters of Poodle located? </Button> 
        <br></br>
        {thirdClicked ? <h3 className="faq" >All of the offices of the company, including the headquarters are located in Bulgaria.</h3> : null}
        <br></br>
        <Button color="info" size="lg" onClick={ () =>  fourthClick()}>How many users does Poodle currently have? </Button> 
        <br></br>
        {fourthClicked ? <h3 className="faq" >A rough estimate of the number of users would be somewhere between 2 and 1 million.</h3> : null}
        <br></br>
        <Button color="info" size="lg" onClick={ () =>  fifthClick()}>What is the price for the premium monthly subscription?</Button> 
        <br></br>
        {fifthClicked ? <h3 className="faq" >It is actually free. We just say that we have a premium subscription so we can filter the people that are highly motivated.</h3> : null}
        <br></br>
        <Button color="info" size="lg" onClick={ () =>  sixthClick()}>Was it hard to come up with so many questions in the FAQs Section.</Button> 
        <br></br>
        {sixthClicked ? <h3 className="faq" >Pretty hard, yes.</h3> : null}
        <br></br>
    </div>
    );
}

export default Faq;