import React, { useState } from "react";
import { BASE_URL } from "../../Common/constants";
import { useHistory } from "react-router";
import validator from "email-validator";
import {Col, Container, Row, Button} from "reactstrap";
import registerBig from "../../Resources/registerBig.png";
import registerSmall from "../../Resources/registerSmall.png";
import './RegisterForm.css';
import TextField from '@material-ui/core/TextField';

const RegisterForm = () => {
  const [error, setError] = useState(null);
  const [registerUser, setRegisterUser] = useState({
    email: "",
    firstName: "",
    lastName: "",
    password: "",
  });

  const history = useHistory();

  const register = ({ email, firstName, lastName, password }) => {

    if (! validator.validate(email)) {
      alert(`Email must be a vlaid!.....`);
      return;
    }
    if (! (/(^[a-zA-Z\d]{3,20}$)/.test(firstName))) {
      alert(`Firstname must contain at least three characters and no digits!.....`);
      return;
    }
    if (! (/(^[a-zA-Z\d]{3,20}$)/.test(lastName))) {
      alert(`Lastname must contain at least three characters and no digits!.....`);
      return;
    }
    if (! (/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,20}$/.test(password))) {
      alert(`Password must contain at least six characters, at least one letter and one digit!`);
      return;
    }

    fetch(`${BASE_URL}/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        firstName,
        lastName,
        password,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data.message) {
          return alert(data.message);
        }
        alert(`You have successfully registered in our forum. Please sign in!`);
        history.push("/auth/login");
      })
      .catch((error) => setError(error.message));
  };

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div className="RegistrationForm">
     <Container className="mt-5">
      <Row>
       <Col lg={4} md={6} sm={12} className='text-center mt-5 p-3'>
       <img className="icon-img" src={registerSmall} alt="icon" />
       <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="email"
        label="Email"
        type="text"
        autoComplete="current-password"
        onChange={(e) => setRegisterUser({ ...registerUser, email: e.target.value })}
       />

       <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="firstName"
        label="firstName"
        type="firstName"
        autoComplete="current-password"
        onChange={(e) => setRegisterUser({ ...registerUser, firstName: e.target.value })}
        />

        <TextField
         variant="outlined"
         margin="normal"
         required
         fullWidth
         name="lastName"
         label="lastName"
         type="lastName"
         autoComplete="current-password"
         onChange={(e) => setRegisterUser({ ...registerUser, lastName: e.target.value })}
        />

        <TextField
         variant="outlined"
         margin="normal"
         required
         fullWidth
         name="password"
         label="password"
         type="password"
         autoComplete="current-password"
         onChange={(e) => setRegisterUser({ ...registerUser, password: e.target.value })} 
        />

        <Button color="secondary"  className="mt-5" type="submit" onClick={() => register(registerUser)}>
          Sign Up
        </Button>
       
       <div className="text-left mt-3">
       <a href='http://localhost:3000/auth/login'>Already have an account? Sign in.</a>
       </div>
      </Col>
      <Col lg={8} md={6} sm={12}>
      <img className="w-100" src={registerBig} alt=""/>
      </Col>
     </Row>
     </Container>

    </div>
  );
};

 export default RegisterForm;
