import React, { useState, useContext } from "react";
import { BASE_URL } from "../../Common/constants";
import { useHistory } from "react-router";
import { AuthContext } from "../../Authentication/Providers/AuthContext";
import { getUserFromToken } from "../../Authentication/Utils/token";
import backgroundLogin from "../../Resources/loginNew.png";
import { Col, Container, Row, Button } from "reactstrap";
import loginAvatar from "../../Resources/loginAvatar.png";
import './LoginForm.css';
import TextField from '@material-ui/core/TextField';


 const LoginForm = () => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { authValue, setUser } = useContext(AuthContext);
  const history = useHistory();

  const triggerLogin = () => {

    if (!email) {
      alert(`Please enter Email!`);
      return;
    }
    if (!password) {
      alert(`Please enter Password!`);
      return;
    }

    fetch(`${BASE_URL}/auth/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data.message) {
          return alert(data.message);
        }

        setUser({
          ...authValue,
          user: getUserFromToken(data.token),
          isLoggedIn: true,
        });
        localStorage.setItem("token", data.token);

      })
      .then(() => history.push("/home"))
      .catch((e) => console.error(e));
  };

  return (
    <div className="wave">

      <Container className="mt-5">
        <Row>
           <Col lg={4} md={6} sm={12} className='text-center mt-5 p-3'>
           <img className="icon-img" src={loginAvatar} alt="icon" />
            <div>
             <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={(e) => setEmail(e.target.value)}
                value={email}
             />

             <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
             />
            </div>
            <Button className="mt-5" color='secondary' variant="primary btn-block" type="submit" onClick={() => triggerLogin()}>Login</Button>
            <div className="text-left mt-3">
            <a href="/about"><small className="reset">Forgot your password?</small></a>
            </div>
          </Col>
          <Col lg={8} md={6} sm={12}>
          <img className="w-100" src={backgroundLogin} alt="" />
          </Col>
        </Row>
     </Container>

    </div>
  );
};

export default LoginForm;