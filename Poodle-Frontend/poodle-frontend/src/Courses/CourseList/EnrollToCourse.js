import React, { useEffect, useState, useContext } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { AuthContext } from "../../Authentication/Providers/AuthContext";
import { Link } from "react-router-dom";
import { useHistory } from "react-router";
import { IconButton } from "@material-ui/core";
import EditOutlinedIcon from "@material-ui/icons/EditOutlined";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";
import { Button } from "reactstrap";
import "./EnrollToCourse.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 1200,
    margin: "0 auto",
    marginTop:'20px'
    
  },
  media: {
    height: 140,
  },
});

 const EnrollToCourse = (props) => {
  const [enrolled, setEnroll] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const { user } = useContext(AuthContext);
  const history = useHistory();
  
  const classes = useStyles();

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${props.id}/enroll`)
      .then((response) => {
        if (!response.ok) {
          throw Error("Resource is not found!");
        }
        return response.json();
      })
      .then((data) => setEnroll(data.map((item) => item.email)))
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [props.id]);

  const enroll = (id, email) => {
    fetch(`${BASE_URL}/courses/${id}/enroll`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        email,
      }),
    })
      .then((response) => {
        return response.json();
      })
      .then((_) => setEnroll([...enrolled, user.email]))
      .catch((error) => {
        setError(error.message);
      });
  };

  const unenroll = (id) => {
    fetch(`${BASE_URL}/courses/${id}/unenroll`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")} `,
      },
    })
      .then((_) => setEnroll(enrolled.filter((email) => email !== user.email)))
      .catch((error) => setError(error.message));
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  const teachersView = () => {
    return (
      <div id="teachersWrap">
        <div className="secondWrap">
          <Card id="cardWrap" className={classes.root}>
            <CardMedia
              className={classes.media}
              image="https://inhabitat.com/wp-content/blogs.dir/1/files/2019/03/shutterstock_521176768ban.jpg"
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                <h4 id="teachersLinkFirst">
                  Author: {props.firstName} {props.lastName}{" "}
                </h4>
                <br></br>
                <Link id="teachersLinkSecond" to={`/courses/${props.id}`}>
                  {props.title}{" "}
                </Link>
              </Typography>
              <Typography
                id="propsDescription"
                variant="body2"
                color="textSecondary"
                component="p"
              >
                {props.description}
              </Typography>
              {props.isPublic ? (
                <p id="fontOnly" class="card-text">
                  Public
                </p>
              ) : (
                <p id="fontOnly" class="card-text">
                  Private
                </p>
              )}
            </CardContent>
            <IconButton
              onClick={() => history.push(`/courses/${props.id}/edit`)}
            >
              <EditOutlinedIcon className="EditIcon" />
            </IconButton>

            <IconButton onClick={() => props.handleDeleteCourse(props.id)}>
              <DeleteOutlineIcon color="secondary" className="DeleteIcon" />
            </IconButton>

            <br></br>
            {!props.isPublic ? (
              <Link
                id="teachersLinkThree"
                to={`/courses/${props.id}/enroll/students`}
              >
                {" "}
                Enroll students into this course{" "}
              </Link>
            ) : null}
            <br></br>
            <Link
              id="teachersLinkFour"
              to={`/courses/${props.id}/enrolled/students`}
            >
              {" "}
              View all enrolled students into this course{" "}
            </Link>

            <div id="fontOnly">
              {" "}
              created: {moment(new Date(props.createdOn)).fromNow()}{" "}
            </div>
          </Card>
        </div>
      </div>
    );
  };

 
  return (
    <div id="studentsWrap">
      {user?.role === "Teacher" && teachersView()}

      {user?.role === "Student" && 
        <div>
          {props.isPublic ? (
            <div id="studentsWrapSecond">
              <Card className={classes.root}>
                <CardMedia
                  className={classes.media}
                  image="https://inhabitat.com/wp-content/blogs.dir/1/files/2019/03/shutterstock_521176768ban.jpg"
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    <Link id="studentLinkSecond" to={`/courses/${props.id}`}>
                      {props.title}{" "}
                    </Link>
                  </Typography>
                  <Typography
                    id="propsDescription"
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {props.description}
                  </Typography>
                  {enrolled.includes(user.email) ? (
                    <div>
                      <Button
                        id="enrollUnenrollButton"
                        color="secondary"
                        type="button"
                        onClick={() => unenroll(props.id)}
                      >
                        Unenroll
                      </Button>
                    </div>
                  ) : (
                    <div>
                      <Button
                        id="enrollUnenrollButton"
                        color="secondary"
                        type="button"
                        onClick={() => enroll(props.id, user.email)}
                      >
                        Enroll
                      </Button>
                    </div>
                  )}
                  <div>
                    {" "}
                    created: {moment(new Date(props.createdOn)).fromNow()}{" "}
                  </div>
                </CardContent>
              </Card>
            </div>
          ) : (
            <div>
              {enrolled.includes(user.email) ? (
                <Card id="cardThree" className={classes.root}>
                  <CardMedia
                    className={classes.media}
                    image="https://inhabitat.com/wp-content/blogs.dir/1/files/2019/03/shutterstock_521176768ban.jpg"
                    title="Contemplative Reptile"
                  />
                     <CardContent>
                     <Typography gutterBottom variant="h5" component="h2">
                      <br></br>
                      <Link id="studentLinkSecond" to={`/courses/${props.id}`}>
                        {props.title}{" "}
                      </Link>
                      </Typography>
                      <Typography
                      id="propsDescription"
                      variant="body2"
                      color="textSecondary"
                      component="p"
                      >
                      {props.description}
                     </Typography>
                     <div>
                      <Button
                        id="enrollUnenrollButton"
                        color="secondary"
                        type="button"
                        onClick={() => unenroll(props.id)}
                      >
                       Unenroll
                      </Button>
                    </div>
                    <div>
                    created: {moment(new Date(props.createdOn)).fromNow()}{" "}
                    </div>
                  </CardContent>
                </Card>
                
              ) : null}
            </div>
          )}
        </div>
      }
    </div>
  );
};

export default EnrollToCourse;
