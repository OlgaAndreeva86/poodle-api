import React, { useState, useEffect, useContext } from "react";
import { BASE_URL } from "../../Common/constants";
import { Loader } from "../../Loader/Loader";
import EnrollToCourse from "./EnrollToCourse";
import { AuthContext } from "../../Authentication/Providers/AuthContext";
import CourseDetails from "../CourseDetails/CourseDetails";
import CreateCourseForm from "../CreateCourse/CreateCourse";
import TextField from "@material-ui/core/TextField";
import ReactPaginate from "react-paginate";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import moment from "moment";
import { Button } from "reactstrap";
import "../../App.css";
import "./CourseList.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 1200,
    margin:'0 auto',
    
  },
  media: {
    height: 140,
  },
});

export const CoursesList = () => {
  const [modal, setModal] = useState(false);
  const [courses, setCourses] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [search, setSearch] = useState("");
  const [students, setStudents] = useState([]);

  const { user } = useContext(AuthContext);

  const [pageNumber, setPageNumber] = useState(0);
  const coursesPerPage = 3;
  const pagesVisited = pageNumber * coursesPerPage;

  const classes = useStyles();

  const toggle = () => {
    setModal(!modal);
  };

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses`)
      .then((response) => response.json())
      .then((data) => setCourses(data))
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    if (!user) {
      return;
    }
    setLoading(true);

    fetch(`${BASE_URL}/courses/visible/students`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => setStudents(data))
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const handleDeleteCourse = (id) => {
    fetch(`${BASE_URL}/courses/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")} `,
      },
    })
      .then((response) => response.json())
      .then((_) => {
        const updatedCourses = courses.filter((course) => course.id !== id);
        setCourses(updatedCourses);
      })
      .catch((error) => setError(error.message));
  };

  const result = () => {
    if (user) {
      if (user.role === "Teacher") {
        return courses;
      }
      return students;
    }
    return courses.filter((course) => course.isPublic === 1);
  };


  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  return (
    <div class="card mb-3 page-wrapper">
      <div>
        <div className="allCourses"> All Courses</div>
        {user?.role === "Teacher" ? (
          <div>
            <Button
              color="secondary"
              className="btn-lg"
              type="button"
              onClick={() => setModal(true)}
            >
              Create Course
            </Button>
            <CreateCourseForm toggle={toggle} modal={modal} />
          </div>
        ) : null}
      </div>

      <div className="wrapTag">
        <TextField
          className="textField"
          variant="outlined"
          margin="normal"
          required
          id="search"
          autoComplete="current-word"
          placeholder="What are you looking for.."
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>

      <div>
        {result()
          .slice(pagesVisited, pagesVisited + coursesPerPage)
          .filter((course) =>
            course.title.toLowerCase().includes(search.toLowerCase())
          )
          .map((course) => (
            <div key={course.id} className="AllCourses">
              {!user && course.isPublic ? (
                <div className="secondWrapper">
                  <Card className={classes.root}>
                    <CardMedia
                      className={classes.media}
                      image="https://inhabitat.com/wp-content/blogs.dir/1/files/2019/03/shutterstock_521176768ban.jpg"
                      title="Contemplative Reptile"
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        <h3>
                          <CourseDetails {...course} />
                        </h3>
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        created: {moment(new Date(course.createdOn)).fromNow()}
                      </Typography>
                    </CardContent>
                  </Card>
                </div>
              ) : (
                <EnrollToCourse
                  {...course}
                  handleDeleteCourse={handleDeleteCourse}
                />
              )}
            </div>
          ))}
      </div>
<br></br>
      <ReactPaginate
        previousLabel={"<<"}
        nextLabel={">>"}
        pageCount={Math.ceil(result().length / coursesPerPage)}
        onPageChange={changePage}
        containerClassName={"paginationButtons"}
        previousLinkClassName={"previousButton"}
        nextLinkClassName={"nextButton"}
        disabledClassName={"pagonationDisabled"}
        activeClassName={"paginationActive"}
      />
    </div>
  );
};

export default CoursesList;
