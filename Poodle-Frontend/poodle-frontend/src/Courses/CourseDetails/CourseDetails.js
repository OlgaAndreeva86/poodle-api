import React from "react";
import "./CourseDetails.css";

const CourseDetails = ({ title, description, createdOn }) => {
  return (
    <div className="CourseTitle">
      <div className="courseDets"> {title} </div>

      <div className="courseDets"> {description} </div>
    </div>
  );
};

export default CourseDetails;
