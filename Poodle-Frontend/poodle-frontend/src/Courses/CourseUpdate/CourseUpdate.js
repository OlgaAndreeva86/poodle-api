import React, { useState, useEffect } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { useHistory } from "react-router";
import backgroundUpdate from "../../Resources/update.jpg";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "reactstrap";
import "./CourseUpdate.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 645,
    margin: "0 auto",
    borderRadius: "5px",
  },
  media: {
    height: 140,
  },
});

const CourseUpdate = (props) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const { id } = props.match.params;
  const history = useHistory();
  const classes = useStyles();
  const [updateCourse, setUpdateCourse] = useState({
    title: "",
    description: "",
    isPublic: "",
  });

  const goBack = () => history.push("/courses");

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.message) {
          throw Error(data.message);
        }
        setUpdateCourse({
          title: data.title,
          description: data.description,
          isPublic: data.isPublic,
        });
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id]);

  const update = (courseData) => {
    if (courseData.title.length < 3 || courseData.description.length < 3) {
      return alert(
        `You should update title and description! The title and description must contain at least three characters!`
      );
    }

    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "Application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(courseData),
    })
      .then((result) => result.json())
      .then(() => alert("You have successfully updated this course!"))
      .then(() => goBack())
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  } 

  return (
    <div
      className="update-course-form page-wrapper"
      style={{
        height:'100vh',
        backgroundImage: `url(${backgroundUpdate})`,
        position: "relative",
        backgroundSize: "cover",
      }}
    >
      <Card className={classes.root}>
        <CardMedia
          className={classes.media}
          image="https://inhabitat.com/wp-content/blogs.dir/1/files/2019/03/shutterstock_521176768ban.jpg"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            <label style={{ fontSize: "20px" }} htmlFor="title">
              {" "}
              Title:{" "}
            </label>
            <br />
            <textarea
              id="courseTitle"
              style={{
                maxWidth: "500px",
                borderRadius: "5px",
                paddingTop: "10px",
                paddingLeft: "10px",
              }}
              rows="2"
              cols="80"
              value={updateCourse.title}
              onChange={(e) =>
                setUpdateCourse({ ...updateCourse, title: e.target.value })
              }
              npm
              star
            ></textarea>
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <label style={{ fontSize: "20px" }} htmlFor="description">
              {" "}
              Description:{" "}
            </label>
            <br />
            <textarea
              id="description"
              style={{
                maxWidth: "500px",
                borderRadius: "5px",
                paddingTop: "10px",
                paddingLeft: "10px",
              }}
              rows="4"
              cols="80"
              value={updateCourse.description}
              onChange={(e) =>
                setUpdateCourse({
                  ...updateCourse,
                  description: e.target.value,
                })
              }
              npm
              star
            ></textarea>
          </Typography>
          <input
            type="checkbox"
            checked={updateCourse.isPublic}
            onChange={(e) =>
              setUpdateCourse({ ...updateCourse, isPublic: e.target.checked })
            }
          />
          {!updateCourse.isPublic ? (
            <label className="sizeTwenty" for="checking">
              {" "}
              private{" "}
            </label>
          ) : (
            <label className="sizeTwenty" for="checking">
              {" "}
              public{" "}
            </label>
          )}
        </CardContent>
      </Card>

      <div>
        <br />
        <Button
          className="updateButton"
          color="secondary"
          type="submit"
          onClick={() => update(updateCourse)}
        >
          {" "}
          Update{" "}
        </Button>
        <Button color="info" type="submit" onClick={() => goBack()}>
          {" "}
          Go back{" "}
        </Button>
      </div>
    </div>
  );
};

export default CourseUpdate;
