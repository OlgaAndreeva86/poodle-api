import React, { useEffect, useState } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { useHistory } from "react-router";
import CourseDetails from "../CourseDetails/CourseDetails";
import Sections from "../../Sections/Sections/Sections/Sections";
import { Button } from "reactstrap";
import backgroundCourse from "../../Resources/backgroundCourse.jpg";
import "./Course.css";

const Course = (props) => {
  const [courseData, setCourseData] = useState({});
  const { id } = props.match.params;

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const history = useHistory();

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses/${id}`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.message) {
          throw Error(data.message);
        }
        setCourseData(data);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id]);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i> An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div className="page-wrapper sections"  
      style={{
      backgroundImage: `url(${backgroundCourse})`,
      position: "relative",
      backgroundSize: "cover",
      hight: "100vh"
    }}>
      <div className="courseData">
        {" "}
        <CourseDetails {...courseData} />{" "}
      </div>
      <br></br>

       <Button
        color="secondary"
        className="btn-lg"
        type="button"
        onClick={() => history.push("/courses")}
       >
        See some more courses
      </Button>
      <br></br>

      <div className="sectionsLine"> Sections: </div>

      <Sections className="sectionsData" {...courseData} />
    </div>
  );
};

export default Course;
