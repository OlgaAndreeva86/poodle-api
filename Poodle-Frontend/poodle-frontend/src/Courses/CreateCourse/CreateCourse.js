import React, { useState, useEffect } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { useHistory } from "react-router";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import "./CreateCourse.css";

const CreateCourseForm = ({ modal, toggle }) => {
  const [courses, setCourses] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const history = useHistory();

  const goBack = () => history.push("/courses");

  const refreshPage = () => {
    window.location.reload();
  };

  const [course, setCourse] = useState({
    title: "",
    description: "",
  });

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses`)
      .then((response) => response.json())
      .then((data) => setCourses(data.map((item) => item.title)))
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const createCourse = ({ title, description }) => {
    if (description.length < 3 || title.length < 3) {
      return alert(
        `You should create title and description! The title and description must contain at least three characters!`
      );
    }
    setLoading(true);

    fetch(`${BASE_URL}/courses`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        title,
        description,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw Error("Course is not available!");
        }
        setLoading(false);
        return response.json();
      })
      .then(() => goBack())
      .catch((error) => {
        setLoading(false);
        setError(error.message);
      });
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  const isIncludeTitle = courses.some(
    (item) => item.toLocaleLowerCase() === course.title.toLocaleLowerCase()
  );

  return (
    <div>
      <Modal className="modal-lg" isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Create your course</ModalHeader>
        <ModalBody>
          <form>
            <div className="form-group">
              <label className="fontOnly">Course Title</label>
              <input
                type="text"
                className="form-control"
                onChange={(e) =>
                  setCourse({ ...course, title: e.target.value })
                }
              />
              {isIncludeTitle && (
                <div className="error">This title has been already used!</div>
              )}
              <label className="fontOnly">Course Description</label>
              <textarea
                rows="5"
                className="form-control"
                onChange={(e) =>
                  setCourse({ ...course, description: e.target.value })
                }
              />
            </div>
          </form>
        </ModalBody>
        <ModalFooter>
          <Button
            color="secondary"
            disabled={isIncludeTitle}
            onClick={() => {
              createCourse(course);
              toggle();
              refreshPage();
            }}
          >
            Create
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default CreateCourseForm;
