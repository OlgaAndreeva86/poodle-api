import React, { useState } from "react";
import { BASE_URL } from "../Common/constants";
import {
  Col,
  Container,
  Row,
  Modal,
  Button,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import aboutBig from "../Resources/aboutBig.png";
import { Alert } from "reactstrap";
import "./About.css";
import { useHistory } from "react-router";
import Rating from "./test/test";

const About = () => {

  const [message, setMessage] = useState(false);
  const [modalOpinion, setModalOpinion] = useState(false);
  const [modalRequest, setModalRequest] = useState(false);
  const history = useHistory();

  const [createOpinion, setCreateOpinion] = useState({
    email: "",
    content: "",
    isRequest: 0,
  });

  const create = (opinionData) => {
    fetch(`${BASE_URL}/users/opinion`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(opinionData),
    })
      .then((result) => result.json())
      .then(() => history.push("/about"));
  };

  const toggleMessage = () => {
    setMessage(!message);
  };

  const toggleOpinion = () => {
    setModalOpinion(!modalOpinion);
  };

  const toggleRequest = () => {
    setModalRequest(!modalRequest);
  };

  return (
    <div className="page-wrapper">
      <Container>
        <Row>
          <Col lg={4} md={6} sm={12} className="text-center p-3">
            <div className="title">
              <h4 style={{ fontFamily: "Nunito" }}>
                {" "}
                We'd love to hear from you
              </h4>
              <br></br>
            </div>
            {message ? (
              <Button
                className="mt-2"
                color="secondary"
                onClick={() => {
                  setModalOpinion(true);
                  setCreateOpinion({ ...createOpinion, isRequest: 0 });
                  toggleMessage();
                }}
              >
                Share your opinion
              </Button>
            ) : (
              <Button
                className="mt-2"
                color="secondary"
                onClick={() => {
                  setModalOpinion(true);
                  setCreateOpinion({ ...createOpinion, isRequest: 0 });
                }}
              >
                Share your opinion
              </Button>
            )}
            <Modal isOpen={modalOpinion} toggle={toggleOpinion}>
              <ModalHeader toggle={toggleOpinion}>
                {" "}
                Share your opinion{" "}
              </ModalHeader>
              <ModalBody>
                <form>
                  <div className="form-group">
                    <label style={{ fontFamily: "Nunito" }}>
                      {" "}
                      Text Your Email
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      value={createOpinion.email}
                      onChange={(e) =>
                        setCreateOpinion({
                          ...createOpinion,
                          email: e.target.value,
                        })
                      }
                    />
                    <label>Content</label>
                    <textarea
                      rows="5"
                      className="form-control"
                      value={createOpinion.content}
                      onChange={(e) =>
                        setCreateOpinion({
                          ...createOpinion,
                          content: e.target.value,
                        })
                      }
                    />
                  </div>
                </form>
              </ModalBody>
              <ModalFooter>
                {!message ? (
                  <Button
                    color="secondary"
                    type="submit"
                    onClick={() => {
                      create(createOpinion);
                      toggleOpinion();
                      toggleMessage();
                    }}
                  >
                    Send
                  </Button>
                ) : (
                  <Button
                    color="secondary"
                    type="submit"
                    onClick={() => {
                      create(createOpinion);
                      toggleOpinion();
                    }}
                  >
                    Send
                  </Button>
                )}
              </ModalFooter>
            </Modal>

            <br></br>

            {message ? (
              <Button
                className="mt-2"
                color="secondary"
                onClick={() => {
                  setModalRequest(true);
                  setCreateOpinion({ ...createOpinion, isRequest: 1 });
                  toggleMessage();
                }}
              >
                Get in touch with us{" "}
              </Button>
            ) : (
              <Button
                className="mt-2"
                color="secondary"
                onClick={() => {
                  setModalRequest(true);
                  setCreateOpinion({ ...createOpinion, isRequest: 1 });
                }}
              >
                Get in touch with us
              </Button>
            )}
            <Modal isOpen={modalRequest} toggle={toggleRequest}>
              <ModalHeader toggle={toggleRequest}>
                {" "}
                Ask your question{" "}
              </ModalHeader>
              <ModalBody>
                <form>
                  <div className="form-group">
                    <label style={{ fontFamily: "Nunito" }}>
                      Text Your Email
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      value={createOpinion.email}
                      onChange={(e) =>
                        setCreateOpinion({
                          ...createOpinion,
                          email: e.target.value,
                        })
                      }
                    />
                    <label style={{ fontFamily: "Nunito" }}>Content</label>
                    <textarea
                      rows="5"
                      className="form-control"
                      value={createOpinion.content}
                      onChange={(e) =>
                        setCreateOpinion({
                          ...createOpinion,
                          content: e.target.value,
                        })
                      }
                    />
                  </div>
                </form>
              </ModalBody>
              <ModalFooter>
                {!message ? (
                  <Button
                    color="secondary"
                    type="submit"
                    onClick={() => {
                      create(createOpinion);
                      toggleRequest();
                      toggleMessage();
                    }}
                  >
                    Send
                  </Button>
                ) : (
                  <Button
                    color="secondary"
                    type="submit"
                    onClick={() => {
                      create(createOpinion);
                      toggleRequest();
                    }}
                  >
                    Send
                  </Button>
                )}
              </ModalFooter>
            </Modal>
          </Col>
          <Col lg={8} md={6} sm={12}>
            <img className="w-100" src={aboutBig} alt="" />
          </Col>
        </Row>
      </Container>

      {message ? (
        <Alert color="success">
          <h4 style={{ fontFamily: "Nunito" }} className="alert-heading">
            Thank you!
          </h4>
          <p style={{ fontFamily: "Nunito" }}>
            {" "}
            Your opinion, tip or recommendation are the very meaning of our
            cause because our purpose is building a community.
          </p>
          <hr />
          <p style={{ fontFamily: "Nunito" }} className="mb-0">
            {" "}
            Please keep sharing with us and we will get in touch as soon as
            possible.{" "}
          </p>
        </Alert>
      ) : null}

      <Button
        style={{ fontSize: "45px", borderRadius: "50px" }}
        className="mt-2"
        color="info"
        onClick={() => {
          history.push("/faqs");
        }}
      >
        FAQs
      </Button>
      <Rating />
    </div>
  );
};

export default About;
