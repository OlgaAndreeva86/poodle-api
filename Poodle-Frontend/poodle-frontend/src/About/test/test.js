import "./test.css";
import StarRateIcon from "@material-ui/icons/StarRate";
import img1 from "../../Resources/fff.jpg";
import img2 from "../../Resources/sss.jpg";
import img3 from "../../Resources/ttt.jpg";

const Rating = () => {
  return (
    <div className="Header">
      <section id="testimonials">
        <div class="testimonials-heading">
          <h1 style={{ fontFamily: "Nunito" }}>Students Say</h1>
        </div>

        <div class="testimonial-box-container">
          <div class="testimonial-box">
            <div class="box-top">
              <div class="profile">
                <div class="profile-img">
                  <img alt="image1" src={img2} />
                </div>
                <div class="name-user">
                  <strong style={{ fontFamily: "Nunito" }}>Milen Kostov</strong>
                  <span style={{ fontFamily: "Nunito" }}>@milenk</span>
                </div>
              </div>
              <div class="reviews">
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
              </div>
            </div>
            <div class="client-comment">
              <p style={{ fontFamily: "Nunito" }}>
                I've been using Poodle for an year and I love it!
              </p>
            </div>
          </div>

          <div class="testimonial-box">
            <div class="box-top">
              <div class="profile">
                <div class="profile-img">
                  <img alt="image2" src={img1} />
                </div>
                <div class="name-user">
                  <strong style={{ fontFamily: "Nunito" }}>
                    Irina Vulkova
                  </strong>
                  <span style={{ fontFamily: "Nunito" }}>@chakpukdatrqbva</span>
                </div>
              </div>
              <div class="reviews">
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
              </div>
            </div>
            <div class="client-comment">
              <p style={{ fontFamily: "Nunito" }}>
                I love ideas that support a good cause. <br></br> Keep it up
                Poodle!
              </p>
            </div>
          </div>

          <div class="testimonial-box">
            <div class="box-top">
              <div class="profile">
                <div class="profile-img">
                  <img alt="image3" src={img3} />
                </div>
                <div class="name-user">
                  <strong style={{ fontFamily: "Nunito" }}>
                    Alexander Kulev
                  </strong>
                  <span style={{ fontFamily: "Nunito" }}>@alexanderkulev</span>
                </div>
              </div>
              <div class="reviews">
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
                <StarRateIcon />
              </div>
            </div>
            <div class="client-comment">
              <p style={{ fontFamily: "Nunito" }}>
                5 out of 5. High quality learning experience.<br></br>{" "}
                Responsive teachers. Amazing platform!
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Rating;
