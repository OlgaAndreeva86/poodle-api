import React, { useState, useEffect, useContext } from "react";
import { Button } from "reactstrap";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { useHistory } from "react-router";
import { AuthContext } from "../../Authentication/Providers/AuthContext";
import backgroundUpdate from "../../Resources/updatee.jpg";
import "./EditUser.css";

const EditUser = () => {
 
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const { user } = useContext(AuthContext);
  const history = useHistory();

  const [editUser, setEditUser] = useState({
    firstName: "",
    lastName: "",
    password: "",
  });

  const goBack = () => history.push("/myprofile");

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/users/${user?.sub}`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.message) {
          throw Error(data.message);
        }
        setEditUser({
          firstName: data.firstName,
          lastName: data.lastName,
          password: "***********",
        });
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [user?.sub]);

  const update = (userData) => {
    if (!/(^[a-zA-Z\d]{3,20}$)/.test(userData.firstName)) {
      alert(
        `Firstname must contain at least three characters and no digits!.....`
      );
      return;
    }
    if (!/(^[a-zA-Z\d]{3,20}$)/.test(userData.lastName)) {
      alert(
        `Lastname must contain at least three characters and no digits!.....`
      );
      return;
    }
    if (!/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,20}$/.test(userData.password)) {
      alert(
        `Password must contain at least six characters, at least one letter and one digit!.....`
      );
      return;
    }
    setLoading(true);

    fetch(`${BASE_URL}/users/${user.sub}`, {
      method: "PUT",
      headers: {
        "Content-Type": "Application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify(userData),
    })
      .then((result) => result.json())
      .catch((error) => setError(error.message))
      .finally(() => {
        setLoading(false);
        goBack();
      });
  };

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div
      className="update-course-form page-wrapper"
      style={{
        backgroundImage: `url(${backgroundUpdate})`,
        position: "relative",
        minHeight: "100vh",
        backgroundSize: "cover",
      }}
    >
      <div className="col-3 m-auto">
        <label className="firstName" htmlFor="firstName">
          {" "}
          First Name:{" "}
        </label>
        <br />
        <input
          className="form-control"
          type="text"
          placeholder="How would you like to change First Name?"
          value={editUser.firstName}
          onChange={(e) =>
            setEditUser({ ...editUser, firstName: e.target.value })
          }
        />
        <br />
        <label className="lastName" htmlFor="lastName">
          {" "}
          Last Name:{" "}
        </label>
        <br />
        <input
          className="form-control"
          type="text"
          placeholder="How would you like to change Last Name?"
          value={editUser.lastName}
          onChange={(e) =>
            setEditUser({ ...editUser, lastName: e.target.value })
          }
        />
        <br />
        <label className="pass" htmlFor="pass">
          {" "}
          Password:{" "}
        </label>
        <br />
        <input
          className="form-control"
          type="text"
          placeholder="How would you like to change Password?"
          value={editUser.password}
          onChange={(e) =>
            setEditUser({ ...editUser, password: e.target.value })
          }
        />

        <br />
        <Button
          className="m-1"
          color="secondary"
          type="submit"
          onClick={() => update(editUser)}
        >
          Update
        </Button>
        <Button
          className="m-1"
          color="info"
          type="submit"
          onClick={() => goBack()}
        >
          Go back
        </Button>
      </div>
    </div>
  );
};

export default EditUser;
