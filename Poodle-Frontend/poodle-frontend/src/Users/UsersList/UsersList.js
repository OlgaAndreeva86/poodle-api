import React, { useEffect, useState } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { Button } from "reactstrap";
import { useStyles } from "../AllEnrolledStudents/useStyles";
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import  TablePagination  from "@material-ui/core/TablePagination";
import TableFooter  from "@material-ui/core/TableFooter";
import './UsersList.css';


  const UsersList = () => {

    const [usersData, setUsersData] = useState([]);
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const classes = useStyles();

    useEffect(() => {
      setLoading(true);

      fetch(`${BASE_URL}/users`, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      })
        .then((response) => response.json())
        .then((data) => setUsersData(data))
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
    }, []);
  
  const deleteUser = (id) => {

      fetch(`${BASE_URL}/users/${id}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")} `,
        },
      })
        .then((response) => response.json())
        .then( _ => {
          const updatedUsers = usersData.filter((user) => user.id !== id);
          setUsersData(updatedUsers);
        })
        .catch((error) => setError(error.message));
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

    if (loading) {
      return <Loader />;
    }

    if (error) {
      return (
        <h4>
          <i>An error has occured: </i>
          {error}
        </h4>
      );
    }
  
    return (
      <div className={'page-wrapper'}>
        <TextField
                variant="outlined"
                margin="normal"
                required
                id="search"
                autoComplete="current-word"
                placeholder='Search user by email...' 
                value={search} 
                onChange={e => setSearch(e.target.value)}
        />
    <div className='wrapper'>
    <TableContainer component={Paper} className = {classes.tableContainer}>
    <Table className={classes.table} stickyHeader aria-label="stcky table">
    <TableHead>
      <TableRow>
      <TableCell style={{width: "300px"}} className={classes.tableHeaderCell}>UserId</TableCell>
      <TableCell className={classes.tableHeaderCell}>Email</TableCell>
      <TableCell className={classes.tableHeaderCell}>FullName</TableCell>
      <TableCell className={classes.tableHeaderCell}>Created On</TableCell>
      <TableCell className={classes.tableHeaderCell}>Delete</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
     {
      usersData
      .filter(user => user.email.toLowerCase().includes(search.toLowerCase()))
      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((user) => (
       <TableRow key={user.id}>
       <TableCell > {user.id}</TableCell>
       <TableCell className={classes.name}>{user.firstName} {user.lastName}</TableCell>
       <TableCell >{user.email}</TableCell>
       <TableCell >{new Date(user.createdOn).toLocaleDateString()} </TableCell>
       <TableCell>
       <Button color="danger" type="submit" onClick={() => deleteUser(user.id)}> Delete </Button>
       </TableCell>
       </TableRow>
      ))}
    </TableBody>
    <TableFooter>
    <TablePagination
    rowsPerPageOptions={[5, 10, 15]}
    component="div"
    count={usersData.length}
    rowsPerPage={rowsPerPage}
    page={page}
    onPageChange={handleChangePage}
    onRowsPerPageChange={handleChangeRowsPerPage}
    />
   </TableFooter>
   </Table>
   </TableContainer>
  </div>
  </div>
    );
  };
  
  export default UsersList;
  

 