import React, { useEffect, useState, useContext } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { AuthContext } from "../../Authentication/Providers/AuthContext";
import "./UsersView.css";

const User = () => {
  const { user } = useContext(AuthContext);

  const [courses, setCourses] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const id = user.sub;

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/courses`)
      .then((response) => response.json())
      .then((data) => {
        const filtered = data.filter((c) => c.userId === +id);
        setCourses(filtered);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [id]);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div className="page-wrapper user-courses">
      <h3 className="hereAre">
        Here are the most recent courses created by you:
      </h3>
      {courses.map((course) => (
        <div key={course.id} className="AllCourses">
          <div className="CourseTitle">
            <div className="courseTitle">{course.title} </div>
            <div className="courseDescription"> {course.description} </div>
            <div className="courseDate">
              {" "}
              created on {new Date(course.createdOn).toLocaleDateString()}{" "}
            </div>
            <br></br>
          </div>
        </div>
      ))}
    </div>
  );
};

export default User;
