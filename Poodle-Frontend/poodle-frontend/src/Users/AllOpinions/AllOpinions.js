import { Col, Container, Row, Modal, ModalBody, Button, ModalHeader, ModalFooter} from "reactstrap";
import React, { useEffect, useState } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import './AllOpinions.css';

  const AllOpinions = () => {
    const [opinionsData, setOpinionsData] = useState([]);
    const [modal, setModal] = useState(false); 
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

  useEffect(() => {
      setLoading(true);

      fetch(`${BASE_URL}/users/opinions/requests`, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      })
        .then((response) => response.json())
        .then((data) => setOpinionsData(data))
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
    }, []);
  
  const deleteOpinion = (id) => {

      fetch(`${BASE_URL}/users/opinions/${id}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")} `,
        },
      })
        .then((response) => response.json())
        .then( _ => {
          const updatedOpinions = opinionsData.filter((opinion) => opinion.id !== id);
          setOpinionsData(updatedOpinions);
        })
        .catch((error) => setError(error.message));
  };
  
    const toggle = () => {
    setModal(!modal);
    }

    if (loading) {
      return <Loader />;
    }
  
    if (error) {
      return (
        <h4>
          <i>An error has occured: </i>
          {error}
        </h4>
      );
    }

    return (
        <Container className={'page-wrapper main-container'} >
        <Row xs="3" sm="2" md="4">
        <Col xs="auto" >
          <h2 className="title">Users requests</h2> 
          { opinionsData.length ?
           opinionsData.map((opinion) => (
            <div key={opinion.id} >
            { opinion.isRequest ?
            <div>
            <div > {opinion.id} </div>
            <div className='opinionEmail'  id="email"> User's email: {opinion.email} </div>
            <div className='opinionContent' > {opinion.content}  </div>
            <div className='opinionDate' > created on: {new Date(opinion.createdOn).toLocaleDateString()} </div>
            <span>
            <Button className='m-1' color="danger" type="submit" onClick={() => deleteOpinion(opinion.id)}> Delete </Button>
            <Button className='m-1' color="secondary"  type="submit" onClick={() => setModal(!modal) }> Response </Button>
            </span>
            <Modal isOpen={modal} toggle={toggle}>
            <ModalHeader toggle={toggle}>Send your responce</ModalHeader>
            <ModalBody>
            <div className = 'form-group'>
            <label className="textUserEmail" > Text User Email</label>
            <input type="text" className = 'form-control'/>
            <label className="yourResponse" > Your Response </label>
            <textarea rows="5" className="form-control" />
            </div>
            </ModalBody>
            <ModalFooter>
            <Button color="secondary" type="submit" onClick={toggle}>Send</Button>
            </ModalFooter>
            </Modal>
            <br></br>
            </div>
            :
            null }
            </div>))
            : null }
          </Col>

          <Col xs="auto">
          <h2  className="title">Users opinions</h2>
          { opinionsData.length ?
           opinionsData.map((opinion) => (
            <div key={opinion.id} >
            { !opinion.isRequest ?
            <div>
            <div className="opinionId" > {opinion.id}  </div>
            <div className="userEmail" id="email"> User's email: {opinion.email}  </div>
            <div className="contentOpinion"> {opinion.content}  </div>
            <div className="dateOpinion"> created on: {new Date(opinion.createdOn).toLocaleDateString()} </div>
            <Button className='m-1' color="danger" type="submit" onClick={() => deleteOpinion(opinion.id)}> Delete </Button>
            <br></br>
            </div>
           :
           null }
            </div>))
            : null }
          </Col>
          </Row>
        </Container> 
    );

  };
  
  export default AllOpinions;
  