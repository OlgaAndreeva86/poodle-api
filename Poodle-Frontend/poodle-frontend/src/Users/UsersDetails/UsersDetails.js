import React from "react";

import "./UserDetails.css";

const UsersDetails = (props) => {
  return (
    <div>
      <div>
        <div></div>
        <div className="userId"> userId: {props.id} </div>
        <div className="userEmail"> Email: {props.email} </div>
        <div className="userName">
          {" "}
          FullName: {props.firstName} {props.lastName}{" "}
        </div>
        <div className="userDate">
          {" "}
          created on: {new Date(props.createdOn).toLocaleDateString()}{" "}
        </div>
      </div>
    </div>
  );
};

export default UsersDetails;
