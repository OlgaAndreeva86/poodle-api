import React, { useEffect, useState } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { useHistory } from "react-router";
import { Button } from "reactstrap";
import { useStyles } from "../AllEnrolledStudents/useStyles";
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import  TablePagination  from "@material-ui/core/TablePagination";
import TableFooter  from "@material-ui/core/TableFooter";
import './EnrollStudents.css';

  const EnrollStudents = (props) => {

    const [studentsData, setStudentsData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [search, setSearch] = useState('');
    const history = useHistory();
    const {id} = props.match.params;

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);

    const classes = useStyles();

    const goBack = () => history.push(`/courses`);

    useEffect(() => {
      setLoading(true);
      
      fetch(`${BASE_URL}/courses/${id}/notenrolled`, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      })
        .then((response) =>{
          if(!response.ok) {
          throw Error("Resource is not found!");
          }
         return response.json()})
        .then((data) => setStudentsData(data))
        .catch((error) => setError(error.message))
        .finally(() => setLoading(false));
    }, [id]);

    const enrollStudent = ( email ) => {
      setLoading(true);

        fetch(`${BASE_URL}/courses/${id}/enroll/teacher`, {
          method: "POST",
          headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
          body: JSON.stringify({
            email,
          }),
          })
          .then((response) => {
           setLoading(false);
           return (response).json()
          
          })
          .then(data => setStudentsData(data.allNotEnrolled))
          .catch((error) => {
           setLoading(false);
           setError(error.message);
          });
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };

    if (loading) {
      return <Loader />;
    }
  
    if (error) {
      return (
        <h4>
          <i>An error has occured: </i>
          {error}
        </h4>
      );
    }
  
    return (
      <div className={'page-wrapper'} >
        <div>
        <TextField
                variant="outlined"
                margin="normal"
                required
                id="search"
                autoComplete="current-word"
                placeholder='Search student by email...' 
                value={search} 
                onChange={e => setSearch(e.target.value)}
        />
    <div className="wrapper">
    <TableContainer component={Paper} className = {classes.tableContainer}>
    <Table className={classes.table} stickyHeader aria-label="stcky table">
    <TableHead>
      <TableRow>
      <TableCell style={{width: "300px"}} className={classes.tableHeaderCell}>UserId</TableCell>
      <TableCell className={classes.tableHeaderCell}>Email</TableCell>
      <TableCell className={classes.tableHeaderCell}>FullName</TableCell>
      <TableCell className={classes.tableHeaderCell}>Created On</TableCell>
      <TableCell className={classes.tableHeaderCell}>Enroll</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
     {
      studentsData
      .filter(student => student.email.toLowerCase().includes(search.toLowerCase()))
      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((student) => (
       <TableRow key={student.id}>
       <TableCell > {student.id}</TableCell>
       <TableCell className={classes.name}>{student.firstName} {student.lastName}</TableCell>
       <TableCell >{student.email}</TableCell>
       <TableCell >{new Date(student.createdOn).toLocaleDateString()} </TableCell>
       <TableCell>
       <Button color="secondary" type="submit" onClick={() => enrollStudent(student.email)}> Enroll </Button>
       </TableCell>
       </TableRow>
      ))}
    </TableBody>
    <TableFooter>
    <TablePagination
    rowsPerPageOptions={[5, 10, 15]}
    component="div"
    count={studentsData.length}
    rowsPerPage={rowsPerPage}
    page={page}
    onPageChange={handleChangePage}
    onRowsPerPageChange={handleChangeRowsPerPage}
   />
   </TableFooter>
   </Table>
   </TableContainer>
   </div>

    </div>
    <br></br>
    <Button color="info" onClick={() => goBack()}> Go Back </Button>
    </div>
    );
  };
  
  export default EnrollStudents;
  
  