import { makeStyles } from '@material-ui/core/styles';


export const useStyles = makeStyles({
  table: {
    minWidth: 950,
  },
   tableContainer: {
    borderRadius: 5,
    margin: '10px 10px',
    maxWidth: 950,

  },
  tableHeaderCell: {
    fontWeight: 'bold',
    backgroundColor: '#adb5bd'
  },
  name: {
    fontWeight: 'bold',
  }
});

