import React, { useEffect, useState, useContext } from "react";
import { BASE_URL } from "../../Common/constants";
import Loader from "../../Loader/Loader";
import { AuthContext } from "../../Authentication/Providers/AuthContext";

const Student = () => {

  const { user } = useContext(AuthContext);

  const [courses, setCourses] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);

    fetch(`${BASE_URL}/users/${user?.sub}/courses`, {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((response) => {
        if (!response.ok) {
          throw Error("You should log in!");
        }
        return response.json();
      })
      .then((data) => setCourses(data))
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, [user?.sub]);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  return (
    <div>
      <h3 style={{marginTop: '20px'}}>Here are the courses you are enrolled for:</h3>
      {courses.map((course) => (
        <div key={course.id} className="AllCourses">
          <div>
            <div style={{ fontWeight: "bold", marginTop: '10px'}}>{course.title} </div>
            <div> {course.description} </div>
            <div>
              {" "}
              created on {new Date(course.createdOn).toLocaleDateString()}{" "}
            </div>
            <br></br>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Student;
