import pool from "./pool.js";

const getAll = async () => {
  const sql =
  `SELECT c.id, c.title, c.description, c.isPublic, c.createdOn, c.userId, u.email, u.firstName, u.lastName, u.roleId
  FROM courses c
  JOIN users u on c.userId = u.id
  WHERE c.isDeleted = 0
  ORDER BY c.createdOn DESC;`;

  return await pool.query(sql);
};

const getBy = async (column, value) => {
  const sql =
  `SELECT c.id, c.title, c.description, c.isPublic, c.createdOn, c.userId, c.isDeleted, u.email, u.firstName, u.lastName, u.roleId
  FROM courses c
  JOIN users u on c.userId = u.id
  WHERE c.isDeleted = 0
  AND ${column} = ?`;

  const result = await pool.query(sql, [value]);

  return result[0];
};

const create = async (title, description, userId) => {
  const sql =
  `INSERT INTO courses(title, description, userId)
   VALUES (?, ?, (SELECT id FROM users WHERE email = ?))`;

  const result = await pool.query(sql, [title, description, userId]);

  return {
    id: result.insertId,
    title: title,
    description: description,
    userId: userId,
  };
};

const update = async (course) => {
  const { id, title, description, isPublic } = course;
  const sql =
  `UPDATE courses 
   SET title = ?, description = ?, isPublic = ?
   WHERE isDeleted = 0
   AND id = ?`;

  return await pool.query(sql, [title, description, isPublic, id]);
};

const remove = async (courseId) => {
  const sql = 
   `UPDATE courses
    SET isDeleted = 1 
    WHERE id = ?`;

  return await pool.query(sql, [courseId]);
};

const enroll = async (courseId, userId) => {
  const sql = 
  `INSERT INTO enrolled(courseId, userId)
   VALUES(?, (SELECT id FROM users WHERE email = ?))`;

  return await pool.query(sql, [courseId, userId]);
};

const unenroll = async (courseId, userId) => {
  const sql = 
  `DELETE FROM enrolled WHERE courseId = ? and userId = ?;`;

  return await pool.query(sql, [courseId, userId]);
};

const getEnrolledStudents = async (courseId) => {
  const sql =
  `SELECT u.id, u.email, u.firstName, u.lastName, u.createdOn
   FROM enrolled e
   LEFT JOIN users u on  e.userId = u.id
   WHERE courseId = ?`;

  return await pool.query(sql, [courseId]);
};

const getNotenrolledStudents = async (courseId) => {
  const sql =
  `SELECT * from users u
   LEFT JOIN enrolled e on u.id = e.userId
   WHERE (u.id IS NULL OR (u.id NOT IN (SELECT e.userId FROM enrolled e WHERE e.courseId = ?))) AND u.isDeleted = 0 AND u.roleId = 2
   GROUP BY u.id;`

  return await pool.query(sql, [courseId]);
};

const getAllVisibleCoursesByStudent = async (id) => {
  const sql =
  `SELECT c.id, c.title, c.isDeleted, c.isPublic, c.description, c.createdOn, e.courseId
  from courses c
  LEFT JOIN enrolled e on  c.id = e.courseId 
  WHERE (c.isPublic = 1 or (e.userId = ? and c.isPublic = 0 )) and c.isDeleted=0
  group by c.id;`

  return await pool.query(sql, [id]);
};

export default {
  getAll,
  getBy,
  create,
  update,
  remove,
  enroll,
  unenroll,
  getEnrolledStudents,
  getNotenrolledStudents,
  getAllVisibleCoursesByStudent
};
