import pool from "./pool.js";

 const getBy = async (column, value) => {
  const sql = 
   `SELECT *
    FROM users
    WHERE ${column} = ?`;

  const result = await pool.query(sql, [value]);
 
  return result[0];
 };

 const getWithRole = async (email) => {
  const sql = 
   `SELECT u.id, u.email, u.password, r.name as role
    FROM users u
    JOIN roles r ON u.roleId = r.id
    WHERE u.email = ?`;

  const result = await pool.query(sql, [email]);

  return result[0];
 };

 const create = async (email, firstName, lastName, password, role) => {
  const sql =
   `INSERT INTO users(email, firstName, lastName, password, roleId)
    VALUES (?,?,?,?,(SELECT id FROM roles WHERE name = ?))`;

  const result = await pool.query(sql, [email, firstName, lastName, password, role]);

  return {
    id: result.insertId,
    email: email,
  };
 };

 const createOpinion = async (email, content, isRequest) => {
  const sql =
   `INSERT INTO opinions(email, content, isRequest)
    VALUES (?,?,?)`;

  const result = await pool.query(sql, [email, content, isRequest]);

  return {
    id: result.insertId,
    email: email,
    content: content,
    isRequest: isRequest
  };
 };

 const getOpinions = async () => {
  const sql = 
   `SELECT *
    FROM opinions
    ORDER BY createdOn DESC;`;
   return await pool.query(sql);
 };

 const update = async (user) => {
  const { id, firstName, lastName, password } = user;
  const sql = 
   `UPDATE users 
    SET firstName = ?, lastName = ?, password = ?
    WHERE isDeleted = 0
    AND id = ?`;

  return await pool.query(sql, [firstName, lastName, password, id]);
 }

 const getUsers = async (column, value) => {
  const sql =
  `SELECT u.id, u.email, u.password, u.firstName, u.lastName, u.createdOn, r.name as role
   FROM users u
   JOIN roles r ON u.roleId = r.id
   WHERE isDeleted = 0
   AND ${column} = ?;`

  return await pool.query(sql, [value]);
 };

 const getCoursesById = async (column, value) => {
  const sql =
  `SELECT e.courseId , c.title, c.description, c.createdOn
   FROM enrolled e
   LEFT JOIN courses c on e.courseId = c.id
   where ${column} = ?;`

  return await pool.query(sql, [value]);
 };


 const remove = async (value) => {
  const sql = 
   `UPDATE users
    SET isDeleted = 1 
    WHERE id = ?`;

  return await pool.query(sql, [value]);
 };


 const removeOpinion = async (value) => {
  const sql = 
   `DELETE FROM opinions where id = ?`;
   await pool.query(sql, [value]);

   return {
    id: value,
  };
 };

 export default {
  getBy,
  getWithRole,
  create,
  createOpinion,
  getOpinions,
  update,
  getUsers,
  getCoursesById,
  remove,
  removeOpinion
 };
