import errors from "./errors.js";
import coursesData from "../data/courses-data.js";

 const getAllSections = (sectionsData) => {
  return async (courseId) => {
    const existingCourse = await coursesData.getBy("c.id", courseId);

    if (!existingCourse) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

    const allCourseSections = await sectionsData.getAll("courseId", courseId);
    if (!allCourseSections.length) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

    return {
      error: null,
      section: allCourseSections,
    };
  };
 };


 const getSectionById = (sectionsData) => {
  return async (courseId, sectionId) => {
    const course = await coursesData.getBy("c.id", courseId);

    if (!course) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

    const section = await sectionsData.getBy("id", sectionId);
    if (!section) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

    return {
      error: null,
      section: section,
    };
  };
 };

 const createSection = (sectionsData) => {
  return async ({ title, content, availableFrom, orderPosition, courseId }) => {
    const existingCourse = await coursesData.getBy("c.id", courseId);

    if (!existingCourse) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

    const newSection = await sectionsData.create(
      title,
      content,
      availableFrom,
      orderPosition,
      courseId
    );

    return { error: null, section: newSection };
  };
 };

  const updateSection = (sectionsData) => {
  return async (id, section, courseId) => {
    const currentCourse = await coursesData.getBy("c.id", courseId);

    if (!currentCourse) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

    const existingSection = await sectionsData.getBy("id", id);

    if (!existingSection) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

    const updatedSection = { ...existingSection, ...section };

    const _ = await sectionsData.update(updatedSection);

    return { error: null, section: updatedSection };
  };
 };

 const deleteSection = (sectionsData) => {
  return async (courseId, sectionId) => {
    const currentCourse = await coursesData.getBy("c.id", courseId);

    if (!currentCourse) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

    const currentSection = await sectionsData.getBy("id", sectionId);
    if (!currentSection) {
      return {
        error: errors.RECORD_NOT_FOUND,
        section: null,
      };
    }

   const _ = await sectionsData.remove("id", sectionId);

    return {
      error: null,
      section: currentSection,
    };
  };
 };

  const updateOrder = (sectionsData) => {
   return async ({ order1, order2 }, courseId) => {
    const _ = await sectionsData.order(order1, order2);

    const result = await sectionsData.getAll('courseId', courseId);

    return {
      error: null,
      section: result,
    };
  };
};

 export default {
  getAllSections,
  getSectionById,
  deleteSection,
  createSection,
  updateSection,
  updateOrder
 };
