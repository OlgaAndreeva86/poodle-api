import { STUDENT_ROLE } from "../../config.js";
import errors from "./errors.js";
import bcrypt from "bcrypt";

const signInUser = (usersData) => {
  return async (email, password) => {
    const user = await usersData.getWithRole(email);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: errors.INVALID_SIGNIN,
        user: null,
      };
    }
    return {
      error: null,
      user: user,
    };
  };
};

const getUserById = (usersData) => {
  return async (id, role, userId) => {
    const user = await usersData.getBy("id", id);

    if (!user) {
      return {
        error: errors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    if (role === STUDENT_ROLE) {
      if (user.id !== userId) {
        return { error: errors.OPERATION_NOT_PERMITTED, user: null };
      }
    }

    return { error: null, user: user };
  };
};


const getCoursesByUserId = (usersData) => {
  return async (userId) => {
  const user = await usersData.getBy("id", userId);

    if (!user) {
      return {
        error: errors.RECORD_NOT_FOUND,
        user: null,
      };
    }

   const allCourses = await usersData.getCoursesById("e.userId", userId);

   return { error: null, user: allCourses };
  };
};


const createUser = (usersData) => {
  return async (userCreate) => {
    const { email, firstName, lastName, password } = userCreate;
    const existingUser = await usersData.getBy("email", email);
    if (existingUser) {
      return {
        error: errors.DUPLICATE_RECORD,
        user: null,
      };
    }
    const passwordHash = await bcrypt.hash(password, 10);

    const user = await usersData.create(
      email,
      firstName,
      lastName,
      passwordHash,
      STUDENT_ROLE
    );

    return { error: null, user: user };
  };
};


 const updateUser = usersData => {
  return async (userUpdate) => {

      const user = await usersData.getBy('id', userUpdate.id);
      if (!user) {
          return {
              error: errors.RECORD_NOT_FOUND,
              user: null
        }
      }

      const updated = { ...user, ...userUpdate };
      const _ = await usersData.update(updated);

      return { error: null, user: updated }
  };
 }

 const deleteUser = (usersData) => {
  return async (id) => {
    const userToDelete = await usersData.getBy("id", id);

    if (!userToDelete) {
      return {
        error: errors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    const _ = await usersData.remove(id);
    return { error: null, user: userToDelete };
  };
 };


 const getAllStudents = (usersData) => {
  return usersData.getUsers("r.name", STUDENT_ROLE);
 };

  
 const createOpinion = (usersData) => {
  return async (opinionCreate) => {
    const { email, content, isRequest } = opinionCreate;

    const opinion = await usersData.createOpinion(email, content, isRequest);

    return { opinion: opinion };
  };
 };

 const getAllOpinions = (usersData) => {
  return usersData.getOpinions();
 };

 const deleteOpinion = (usersData) => {
  return async (id) => {
  const opinionToDelete = await usersData.removeOpinion(id);

  return { opinion: opinionToDelete };
  };
 };

 export default {
  signInUser,
  createUser,
  updateUser,
  deleteUser,
  getAllStudents,
  getUserById,
  getCoursesByUserId,
  createOpinion,
  getAllOpinions,
  deleteOpinion
 };
