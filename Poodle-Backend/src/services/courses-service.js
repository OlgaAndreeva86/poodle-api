import errors from "./errors.js";

 const getAllCourses = (coursesData) => {
  return coursesData.getAll();
 };

 const getCourseById = (coursesData) => {
  return async (id) => {
    const course = await coursesData.getBy("c.id", id);

    if (!course) {
      return {
        error: errors.RECORD_NOT_FOUND,
        course: null,
      };
    }

    return { error: null, course: course };
  };
 };

 const createCourse = (coursesData) => {
  return async (coursesCreate) => {
    const { title, description, userId } = coursesCreate;

    const existingCourse = await coursesData.getBy("c.title", title);

    if (existingCourse) {
      return {
        error: errors.DUPLICATE_RECORD,
        course: null,
      };
    }

    const course = await coursesData.create(title, description, userId);

    return { error: null, course: course };
  };
 };

 const updateCourse = (coursesData) => {
  return async (courseUpdate) => {
    const course = await coursesData.getBy("c.id", courseUpdate.id);

    if (!course) {
      return {
        error: errors.RECORD_NOT_FOUND,
        course: null,
      };
    }

    const updated = { ...course, ...courseUpdate };
    const _ = await coursesData.update(updated);

    return { error: null, course: updated };
  };
 };

 const deleteCourse = (coursesData) => {
  return async (id) => {
    const courseToDelete = await coursesData.getBy("c.id", id);
    if (!courseToDelete) {
      return {
        error: errors.RECORD_NOT_FOUND,
        course: null,
      };
    }

    const _ = await coursesData.remove(id);

    return { error: null, course: courseToDelete };
  };
 };

 const enrollStudent = (coursesData) => {
  return async (courseId, userId) => {
    const courseToEnroll = await coursesData.getBy("c.id", courseId);

    if (!courseToEnroll) {
      return {
        error: errors.RECORD_NOT_FOUND,
        enroll: null,
      };
    }

    try {
      const _ = await coursesData.enroll(courseId, userId);
    } catch (e) {
      return {
        error: errors.DUPLICATE_RECORD,
        enroll: null,
      };
    }

    const allNotEnrolled = await coursesData.getNotenrolledStudents(courseId);

    return { error: null, enroll: { ...courseToEnroll, allNotEnrolled } };
  };
 };

 const getAllEnrolledByCourse = (coursesData) => {
  return async (courseId) => {
    const existingCourse = await coursesData.getBy("c.id", courseId);

    if (!existingCourse) {
      return {
        error: errors.RECORD_NOT_FOUND,
        enroll: null,
      };
    }

    const allEnrolled = await coursesData.getEnrolledStudents(courseId);

    if (!allEnrolled) {
      return {
        error: errors.RECORD_NOT_FOUND,
        enroll: null,
      };
    }

    return {
      error: null,
      enroll: allEnrolled,
    };
  };
 };

 const getAllNotenrolledByCourse = (coursesData) => {
  return async (courseId) => {
    const existingCourse = await coursesData.getBy("c.id", courseId);

    if (!existingCourse) {
      return {
        error: errors.RECORD_NOT_FOUND,
        unenroll: null,
      };
    }

    let allUnenrolled = await coursesData.getNotenrolledStudents(courseId);

    if (!allUnenrolled) {
      return {
        error: errors.RECORD_NOT_FOUND,
        unenroll: null,
      };
    }

    return {
      error: null,
      unenroll: allUnenrolled,
    };
  };
 };

 const unenrollStudent = (coursesData) => {

  return async (courseId, userId) => {
    const _ = await coursesData.unenroll(courseId, userId);
  };
 };
 

 const getAllVisibleCourses = (coursesData) => {
    return async (id) => {

    const allVisible = await coursesData.getAllVisibleCoursesByStudent(id);

    return {allVisible: allVisible}
  };
 };



 export default {
  getAllCourses,
  getCourseById,
  createCourse,
  updateCourse,
  deleteCourse,
  enrollStudent,
  getAllEnrolledByCourse,
  getAllNotenrolledByCourse,
  unenrollStudent,
  getAllVisibleCourses
 };
