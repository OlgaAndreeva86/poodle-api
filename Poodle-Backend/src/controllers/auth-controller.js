import express from "express";
import createToken from "../auth/create-token.js";
import errors from "../services/errors.js";
import usersService from "../services/users-service.js";
import usersData from "../data/users-data.js";

const authController = express.Router();

authController.post("/login", async (req, res) => {
  const { email, password } = req.body;
  const { error, user } = await usersService.signInUser(usersData)(
    email,
    password
  );

  if (error === errors.INVALID_SIGNIN) {
    res.status(400).send({
      message: "Invalid email or password!",
    });
  } else {
    const payload = {
      sub: user.id,
      email: user.email,
      role: user.role,
    };
    const token = createToken(payload);
    res.status(200).send({
      token: token,
    });
  }
});

export default authController;
