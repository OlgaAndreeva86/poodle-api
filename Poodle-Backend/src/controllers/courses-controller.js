import express from "express";
import { STUDENT_ROLE, TEACHER_ROLE } from "../../config.js";
import { authMiddleware, roleMiddleware } from "../auth/auth-middleware.js";
import coursesData from "../data/courses-data.js";
import coursesService from "../services/courses-service.js";
import errors from "../services/errors.js";
import createCourseSchema from "../validators/create-course-validator.js";
import updateCourseSchema from "../validators/update-course-validator.js";
import bodyValidator from "../validators/validate-body.js";

const coursesController = express.Router();

coursesController

  // get all courses
  .get("/", async (req, res) => {
    const courses = await coursesService.getAllCourses(coursesData);
    res.status(200).send(courses);
  })

  // get course by id
  .get("/:id", authMiddleware, roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]), async (req, res) => {
    const id = +req.params.id;

    const isValid = /^[0-9]+$/;
    if(!isValid.test(id)) {
      res.status(404).send({ message: "Resource is not found!" });
    }

    const { error, course } = await coursesService.getCourseById(coursesData)(id);
   
   if (error === errors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: "Course is not found!" });
     } else {
        res.status(200).send(course);
      }
    }
  )

   // get all visible courses by sudents
    .get("/visible/students", authMiddleware, roleMiddleware([STUDENT_ROLE]), async (req, res) => {
      const id = req.user.id;

      const { allVisible } = await coursesService.getAllVisibleCourses(coursesData)(id);
     
      res.status(200).send(allVisible);
    }
    )

  // create course
  .post("/", authMiddleware, roleMiddleware([TEACHER_ROLE]), bodyValidator(createCourseSchema), async (req, res) => {
      const createData = req.body;
      createData.userId = req.user.email;

      const { error, course } = await coursesService.createCourse(coursesData)(createData);

      if (error === errors.DUPLICATE_RECORD) {
        res.status(409).send({ message: "Course is not available!" });
      } else {
        res.status(201).send(course);
      }
    }
  )

  // update course by id
  .put("/:id", authMiddleware, roleMiddleware([TEACHER_ROLE]), bodyValidator(updateCourseSchema), async (req, res) => {
      const updateData = req.body;
      updateData.id = +req.params.id;

      const { error, course } = await coursesService.updateCourse(coursesData)(updateData);

      if (error === errors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: "Course is not found!" });
      } else if (error === errors.DUPLICATE_RECORD) {
        res.status(409).send({ message: "Course is not available!" });
      } else {
        res.status(200).send(course);
      }
    }
  )

  // delete course by id
  .delete("/:id", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
      const id = +req.params.id;
      const { error, course } = await coursesService.deleteCourse(coursesData)(id);

      if (error === errors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: "Course is is not found!" });
      } else {
        res.status(200).send(course);
      }
    }
  )

  // enroll student by yourself into a course
  .post("/:id/enroll", authMiddleware, roleMiddleware([STUDENT_ROLE]), async (req, res) => {
      const  id  = +req.params.id;
      const userId = req.user.email;

      const { error, enroll } = await coursesService.enrollStudent(coursesData)(id, userId);

      if (error === errors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: "You are trying to enroll a student into a course that doesn't exist!" });
      }

      if (error === errors.DUPLICATE_RECORD) {
        return res.status(409).send({ message: "You have already enrolled!" });
      }
      res.status(200).send(enroll);
    }
  )
  
  // enroll student by teacher into a course
  .post("/:id/enroll/teacher", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
      const id = +req.params.id;
      const userId = req.body.email;

      const { error, enroll } = await coursesService.enrollStudent(coursesData)(id, userId);

      if (error === errors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: "You are trying to enroll a student into a course that doesn't exist!" });
      }

      if (error === errors.DUPLICATE_RECORD) {
        return res.status(409).send({ message: "You have already enrolled this student!" });
      }
      res.status(200).send(enroll);
      }
  )

  // get enrolled students by course id
    .get("/:id/enroll", async (req, res) => {
      const id = +req.params.id;

      const isValid = /^[0-9]+$/;
      if(!isValid.test(id)) {
        res.status(404).send({ message: "Resource is not found!" });
      }

      const { error, enroll } = await coursesService.getAllEnrolledByCourse(coursesData)(id);

      if (error === errors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: "There are not enrolled students into this course!" });
      }
      res.status(200).send(enroll);
    }
  )

  // get not enrolled students by course id
    .get("/:id/notenrolled", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
      const id = +req.params.id;

      const isValid = /^[0-9]+$/;
      if(!isValid.test(id)) {
        res.status(404).send({ message: "Resource is not found!" });
      }

      const { error, unenroll } = await coursesService.getAllNotenrolledByCourse(coursesData)(id);

      if (error === errors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: "All students are enrolled into this course!" });
      }
      res.status(200).send(unenroll);
    }
  )

  // teacher unenroll student by id
  .delete("/:id/unenroll/:userId", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
    const { id, userId } = req.params;
    await coursesService.unenrollStudent(coursesData)(+id, +userId);

    res.status(200).send({ message : "The student is unenrolled from this course!"});
  })

  // unenroll student by id
  .delete("/:id/unenroll", authMiddleware, roleMiddleware([STUDENT_ROLE]), async (req, res) => {
    const { id } = req.params;
    const userId = req.user.id;

    await coursesService.unenrollStudent(coursesData)(+id, userId);

    res.status(200).send({ message : "The student is unenrolled from this course!"});
  });

 export default coursesController;
