import express from "express";
import { STUDENT_ROLE, TEACHER_ROLE } from "../../config.js";
import { authMiddleware, roleMiddleware } from "../auth/auth-middleware.js";
import sectionsData from "../data/sections-data.js";
import sectionsService from "../services/sections-service.js";
import errors from "../services/errors.js";
import bodyValidator from "../validators/validate-body.js";
import createSectionSchema from "../validators/create-section-validator.js";
import sanitizeHTML from 'sanitize-html';

 const sectionsController = express.Router();

 sectionsController

  // get all sections
  .get("/:id/sections", authMiddleware, roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]), async (req, res) => {
  
    const  id  = +req.params.id;

    const { error, section } = await sectionsService.getAllSections(sectionsData)(id);

    if (error === errors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: "Section is not found!" });
    } else {
      res.status(200).send(section);
    }
   })

  // get section by id
    .get("/:id/sections/:sectionId", authMiddleware, roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]), async (req, res) => {
      const id = +req.params.id;
      const sectionId = +req.params.sectionId;

      const isValid = /^[0-9]+$/;
      if(!isValid.test(id) || !isValid.test(sectionId)) {
        res.status(404).send({ message: "Resource is not found!" });
      }

      const { error, section } = await sectionsService.getSectionById(sectionsData)(id, sectionId);

      if (error === errors.RECORD_NOT_FOUND) {
        return res.status(404).json({ message: "Course or section is not found!" });
      }
      res.status(200).json(section);
    }
   )

  // create section
  .post("/:id/sections", authMiddleware, roleMiddleware([TEACHER_ROLE]), bodyValidator(createSectionSchema), async (req, res) => {

     const createSection = req.body;
     createSection.courseId = +req.params.id;

     const cleanContent = sanitizeHTML(req.body.content, {
      allowedTags: sanitizeHTML.defaults.allowedTags.concat([ 'iframe' ]),
      allowedAttributes: {
         'iframe': ['src', 'height', 'width', 'allowfullscreen'],
        },
      allowedIframeHostnames: ['www.youtube.com', 'player.vimeo.com'],
     });

     createSection.content = cleanContent;

     const { error, section } = await sectionsService.createSection(sectionsData)(createSection);

      if (error === errors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: "Course is not found!" });
      } else {
        res.status(201).send(section);
      }
    }
  )

  // create new sections order
  .post('/:id/sections/order', authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
      const updatedOrder = req.body;
      const courseId = +req.params.id;

      const { error, section } = await sectionsService.updateOrder(sectionsData)(updatedOrder, courseId);

      if (error === errors.RECORD_NOT_FOUND) {
          res.status(404).send({ message: 'Course or section not found!' });
      } else {
          res.status(200).send(section);
      }
   },
   )

  // update section by id
  .put("/:id/sections/:sectionId", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
      const updatedSection = req.body;
      const { id, sectionId } = req.params;

      const cleanContent = sanitizeHTML(req.body.content, {
        allowedTags: sanitizeHTML.defaults.allowedTags.concat([ 'iframe' ]),
        allowedAttributes: {
           'iframe': ['src', 'height', 'width', 'allowfullscreen'],
          },
        allowedIframeHostnames: ['www.youtube.com', 'player.vimeo.com'],
        });
  
       updatedSection.content = cleanContent;

      const { error, section } = await sectionsService.updateSection(sectionsData)(+sectionId, updatedSection, +id);

      if (error === errors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: "Course or section is not found!" });
      } else {
        res.status(200).send(section);
      }
    }
   )

  // delete section by id
  .delete("/:id/sections/:sectionId", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
      const { id, sectionId } = req.params;

      const { error, section } = await sectionsService.deleteSection(sectionsData)(+id, +sectionId);

      if (error === errors.RECORD_NOT_FOUND) {
        return res
          .status(404)
          .json({ message: "Course or section is not found!" });
      }
      res.status(200).json(section);
    }
   );

 export default sectionsController;
