import express from "express";
import usersData from "../data/users-data.js";
import { STUDENT_ROLE, TEACHER_ROLE } from "../../config.js";
import { authMiddleware, roleMiddleware } from "../auth/auth-middleware.js";
import usersService from "../services/users-service.js";
import errors from "../services/errors.js";
import createUserSchema from "../validators/create-user-validator.js";
import updateUserSchema from "../validators/update-user-validator.js";
import bodyValidator from "../validators/validate-body.js";
import bcrypt from "bcrypt";

const usersController = express.Router();

usersController

  // get all students
  .get("/", authMiddleware, roleMiddleware([TEACHER_ROLE]),async (req, res) => {
    const students = await usersService.getAllStudents(usersData)
    res.status(200).send(students);
  })
  
  // get user by id
  .get("/:id", authMiddleware, roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]), async (req, res) => {
    const id = +req.params.id;
    const role = req.user.role;
    const userId = req.user.id;

    const isValid = /^[0-9]+$/;
    if(!isValid.test(id)) { 
      res.status(404).send({ message: "Resource is not found!" });
    }
  
    const { error, user } = await usersService.getUserById(usersData)(id, role, userId);

    if (error === errors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: "User is not found!" });
    } else if (error === errors.OPERATION_NOT_PERMITTED) {
      res.status(400).send({ message: "You are not allowed to view this user!" });
    } else {
      res.status(200).send(user);
    }
  }
 )

   // get all courses by userId
   .get("/:id/courses", authMiddleware, roleMiddleware([STUDENT_ROLE]), async (req, res) => {
    const id = +req.params.id;
 
    const isValid = /^[0-9]+$/;
    if(!isValid.test(id)) {
      res.status(404).send({ message: "Resource is not found!" });
    }
    const { error, user } = await usersService.getCoursesByUserId(usersData)(id);

    if (error === errors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: "User is not found!" });
    } else {
      res.status(200).send(user);
    }
  }
 )

  // create user
  .post("/", bodyValidator(createUserSchema), async (req, res) => {
    const createData = req.body;

    const { error, user } = await usersService.createUser(usersData)(createData);

    if (error === errors.DUPLICATE_RECORD) {
      res.status(409).send({ message: "Email is not available!" });
    } else {
      res.status(201).send(user);
    }
  })

  // update user by id
  .put('/:id', authMiddleware, roleMiddleware([STUDENT_ROLE, TEACHER_ROLE]), bodyValidator(updateUserSchema), async (req, res) => {
      const updateData = req.body;
      updateData.id = +req.params.id;

      const passwordHash = await bcrypt.hash(updateData.password, 10);
      updateData.password = passwordHash;

      const { error, user } = await usersService.updateUser(usersData)(updateData);

      if (error === errors.RECORD_NOT_FOUND) {
          res.status(404).send({ message: 'User is not found!' });
      } else if (updateData.id !== req.user.id) {
          res.status(400).send({ message: 'You are not allowed to update other users!' });
      } else {
          res.status(200).send(user);
      }
  })


  // delete user by id
    .delete("/:id", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
        const  id  = +req.params.id;
        const { error, user } = await usersService.deleteUser(usersData)(id);
  
        if (error === errors.RECORD_NOT_FOUND) {
          res.status(404).send({ message: "User is not found!" });
        } else {
          res.status(200).send(user);
        }
      }
    )

  // create opinion
    .post("/opinion", async (req, res) => {
    const createData = req.body;
    const { opinion } = await usersService.createOpinion(usersData)(createData);
    res.status(201).send(opinion);

   })

  // get all opinions
    .get("/opinions/requests", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
    const opinions = await usersService.getAllOpinions(usersData);
    res.status(200).send(opinions);
  })

   // delete opinion by id
     .delete("/opinions/:id", authMiddleware, roleMiddleware([TEACHER_ROLE]), async (req, res) => {
      const  id  = +req.params.id;
      const { opinion } = await usersService.deleteOpinion(usersData)(id);
        res.status(200).send(opinion);
      }
  );

 export default usersController;
