export default {
  title: (value) => typeof value === "string" && value.length > 3 ,
  content: (value) => typeof value === "string" && value.length > 0,
};