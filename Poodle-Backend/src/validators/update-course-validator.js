export default {
    title: (value) => typeof value === "string" && value.length > 3 ,
    description: (value) => typeof value === "string" && value.length > 3,
  };