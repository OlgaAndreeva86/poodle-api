export default {
  firstName: (value) => /(^[a-zA-Z\d]{3,20}$)/.test(value),
  lastName: (value) => /(^[a-zA-Z\d]{3,20}$)/.test(value),
  password: (value) => /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,20}$/.test(value),
};
